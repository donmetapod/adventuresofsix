﻿using UnityEngine;
using System.Collections;

public class PinchZoomAwais : MonoBehaviour {

	// lerp var
	float lerpTime = 1f;
	float currentLerpTime;
	//ne var
	private bool isTwoFing = false;
	private float startTouchMagnitude;
	private float startTouchZoom;
	private float targetZoom;
	public float minZoom2 = 2.0f;
	public float maxZoom2 = 5.0f;
	private float zoomSpeed = 40f;

	private bool invertMoveX = false;
	private bool invertMoveY = false;

	private Transform target;

	private bool isMousePressed = false;


	float deltaMagnitudeDiff;
	private float orthoZoomSpeed = 0.01f;        // The rate of change of the orthographic size in orthographic mode.
	private float orthoCamSize;
	private Vector3 camPos;
	private float maxZoom;
	private float minZoom = 2;
	private float panSpeed = -0.1f;
	public float ScreenWidth;
	public float SideMenuWidth;
	public float topRightX;
	public static bool isPanning;        // Is the camera being panned?
	
	Vector3 bottomLeft;
	Vector3 topRight;
	
	float cameraMaxY;
	float cameraMinY;
	float cameraMinZ;
	float cameraMaxZ;
	float cameraMaxX;
	float cameraMinX;

	private Vector3 pos;



	void Start()
	{
		pos = transform.position;
		ScreenWidth = Screen.width;
		//SideMenuWidth = Screen.width * 0.1953f;
		topRightX = ScreenWidth;
		isPanning = false;
		maxZoom = Camera.main.orthographicSize;
		orthoCamSize = maxZoom;
		camPos = Camera.main.transform.position;
		
		//set max camera bounds (assumes camera is max zoom and centered on Start)
		topRight = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(topRightX, GetComponent<Camera>().pixelHeight, -transform.position.z));
		bottomLeft = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(0,0,-transform.position.z));
		cameraMaxX = topRight.x;
		cameraMaxY = topRight.y;
		cameraMaxZ = topRight.z;
		cameraMinX = bottomLeft.x;
		cameraMinY = bottomLeft.y;
		cameraMinZ = bottomLeft.z;
	}
	
	void Update ()
	{

		
		#if UNITY_EDITOR
		//click and drag
		if(Input.GetMouseButton(0)){
			isPanning = true;
			float x = Input.GetAxis("Mouse X") * panSpeed;
			float y = Input.GetAxis("Mouse Y") * panSpeed;
			//float z = Input.GetAxis("Mouse Y") * panSpeed;
		//	print("MyX"+x);
			transform.Translate(x,y,0);
		//	transform.position = Vector3.Lerp(transform.position,new Vector3(x,y,0),0.1f);
		//	pos = transform.position;
			isPanning = false;
		}
		#endif
		
//		

		
		#if UNITY_EDITOR
		//zoom
		if((Input.GetAxis("Mouse ScrollWheel") > 0) && Camera.main.orthographicSize > minZoom ) // forward
		{
			//Camera.main.orthographicSize = (Camera.main.orthographicSize - orthoZoomSpeed);
			Camera.main.orthographicSize =Mathf.Lerp (Camera.main.orthographicSize,Camera.main.orthographicSize - orthoZoomSpeed,Time.deltaTime*100f);
		}
		
		if ((Input.GetAxis("Mouse ScrollWheel") < 0) && Camera.main.orthographicSize < maxZoom) // back          
		{
			Camera.main.orthographicSize =Mathf.Lerp (Camera.main.orthographicSize,Camera.main.orthographicSize + orthoZoomSpeed,Time.deltaTime*100f);
		}
		#endif

		Touch[] touches = Input.touches;
		if (touches.Length > 0) {
			if (touches.Length == 2) { // only check when 2 fingers are pressed
				if (Input.touches [1].phase == TouchPhase.Began) { // check that the second finger just began touching
					isTwoFing = true;
					// remember the distance between the two fingers
					startTouchMagnitude = (Input.touches [0].position - Input.touches [1].position).magnitude;
					// remember the current zoom/orthogrphic size of the camera, we use this to interpolate the zoom smoothly
					startTouchZoom = Camera.main.orthographicSize;
				}
//			if (Input.touches [1].phase == TouchPhase.Ended && Input.touches [0].phase == TouchPhase.Ended) {
//				isTwoFing = false;
//			}
				// calculate the relative change since last frame
				float relativeMagnitudeChange = startTouchMagnitude / (Input.touches [0].position - Input.touches [1].position).magnitude;
				targetZoom = startTouchZoom * relativeMagnitudeChange;
				// Limit the zoom between min and max
				targetZoom = Mathf.Clamp (targetZoom, minZoom2, maxZoom2);

				// smoothly zoom in and out
				Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, targetZoom, zoomSpeed*Time.deltaTime);
				//StartCoroutine (waiteMe ());

			}  
			// One Finger Pan
			if (touches.Length == 1) {
				isPanning = true;
				//            Touch touchZero = Input.GetTouch(0);
				//            float x = touchZero.position.x * panSpeed;
				//            float y = touchZero.position.y * panSpeed;
			
//				float x = Input.GetAxis ("Mouse X") * panSpeed;
//				float y = Input.GetAxis ("Mouse Y") * panSpeed;
//				transform.Translate (x * Time.deltaTime * 50f, y * Time.deltaTime * 50f, 0);
//				isPanning = false;
				if (touches [0].phase == TouchPhase.Moved) {
					Vector2 delta = touches [0].deltaPosition;
					float positionX = delta.x * 0.1f * Time.deltaTime;
					positionX = invertMoveX ? positionX : positionX * -1;

					float positionY = delta.y * 0.1f * Time.deltaTime;
					positionY = invertMoveY ? positionY : positionY * -1;

					Camera.main.transform.position += new Vector3 (positionX, positionY, 0);
					//target.position = new Vector3(positionX, positionY, 0);
					//Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position,target.position,10f*Time.deltaTime);
			
			
			
			
				}
			}
		}
		/*
		// 2 finger Zoom
		if (Input.touchCount == 2){
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.

			 deltaMagnitudeDiff = Mathf.Lerp(deltaMagnitudeDiff,prevTouchDeltaMag - touchDeltaMag,10f);
			
			// If the camera is orthographic...
			if (camera.isOrthoGraphic)
			{
				// ... change the orthographic size based on the change in distance between the touches.
			//	camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
				Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize,deltaMagnitudeDiff,Time.deltaTime*100f);
				
				// Make sure the orthographic size never drops below zero.
				Camera.main.orthographicSize = Mathf.Max(camera.orthographicSize, minZoom);
				
				// Make sure the orthographic size never goes above original size.
				Camera.main.orthographicSize = Mathf.Min(camera.orthographicSize, maxZoom);
			
			}
		}*/
		
		
		// On double tap image will be set at original position and scale
		else if(Input.touchCount==1 && Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(0).tapCount==2)
		{
			//camera.orthographicSize = orthoCamSize;
			//Camera.main.transform.position = camPos;
		}
		
		
		//check if camera is out-of-bounds, if so, move back in-bounds
		topRight = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(topRightX, GetComponent<Camera>().pixelHeight, -transform.position.z));
		bottomLeft = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(0,0,-transform.position.z));
		
		if(topRight.x > cameraMaxX)
		{
			transform.position = new Vector3(transform.position.x - (topRight.x - cameraMaxX), transform.position.y, transform.position.z);
		}
		if(topRight.y > cameraMaxY)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y- (topRight.y - cameraMaxY), transform.position.z);
		}
//		if(topRight.z > cameraMaxZ)
//		{
//			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - (topRight.z - cameraMaxZ));
//		}

		if(bottomLeft.x < cameraMinX)
		{
			transform.position = new Vector3(transform.position.x + (cameraMinX - bottomLeft.x), transform.position.y, transform.position.z);
		}
		
		if(bottomLeft.y < cameraMinY)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y + (cameraMinY - bottomLeft.y), transform.position.z);
		}
//		if(bottomLeft.z < cameraMinZ)
//		{
//			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + (cameraMinZ - bottomLeft.z));
//		}
		
		// If back button press andriod
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
		
	}

	void FixedUpdate(){


	}

	IEnumerator waiteMe(){
		yield return new WaitForSeconds (0.5f);
		isTwoFing = false;
	}
}

﻿private var dist : float;
 private var toDrag : Transform;
 private var dragging = false;
 private var offset : Vector3;
 public var selectedObject: GameObject;
 
 function Update() {
     if(Input.GetMouseButtonDown(0))
     {
         var hit : RaycastHit;
         var v3 : Vector3;
         var ray : Ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
         if(Physics.Raycast(ray,hit))
         {
            if(hit.transform.gameObject == selectedObject)
             {
                  toDrag = hit.transform;
                  dist = hit.transform.position.z - Camera.main.transform.position.z;
                  v3 = Vector3(Input.mousePosition.x, Input.mousePosition.y, dist);
                  v3 = Camera.main.ScreenToWorldPoint(v3);
                  offset = toDrag.position - v3;
                  dragging = true;
             }
         }
     }
     if (Input.GetMouseButton(0))
     {
         if (dragging)
         {
             v3 = Vector3(Input.mousePosition.x, Input.mousePosition.y, dist);
             v3 = Camera.main.ScreenToWorldPoint(v3);
             toDrag.position = v3 + offset;
         }
     }
     if (Input.GetMouseButtonUp(0))
     {
         dragging = false;
     }
 }
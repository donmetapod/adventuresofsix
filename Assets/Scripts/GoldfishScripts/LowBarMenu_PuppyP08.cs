﻿using UnityEngine;
using System.Collections;

public class LowBarMenu_PuppyP08 : MonoBehaviour {




	/// <summary>
	/// DrawTextures All for Menu	/// </summary>

	public Texture homeText;
	public Texture restartText;
	public Texture settingText;
	public Texture SkipText;

	public Rect homeTextRect;
	public Rect restartTextRect;
	public Rect settingTextRect;
	public Rect SkipTextRect;


	/// <summary>
	/// T/////end	/// </summary>
	private float screenWidth = 1280f;
	private float screenHeight = 800f;
	public Rect touchButtonNextPOs;
	public GUIStyle touchButtonStyle;

	public Texture BG1Img;
	public Rect BG1Pos;



	public Texture text1Img;
	public Rect text1Pos;

	public GUIStyle nextStyle;
	public Rect nextPos;
	//Menu Variables
	private bool isMenu = false;

	public Texture menuBG;
	public Rect menuBGPos;

	public GUIStyle menuIconStyle;
	public Rect menuIconPos;
	public GUIStyle menuIconStyle2;
	public Rect menuIconPos2;

	public GUIStyle SkipStyle;
	public Rect SkipPos;

	public GUIStyle restartStyle;
	public Rect restartPos;

	public GUIStyle settingStyle;
	public Rect settingPos;

	public GUIStyle homeStyle;
	public Rect homePos;

	public GUIStyle crossStyle;
	public Rect crossPos;
	//end off Menu variables
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

//		#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
//		if(Input.GetKeyDown("right")){
//			Application.LoadLevel(3);
//		}
//		#else
//		for (var i = 0; i < Input.touchCount; ++i) {
//			Touch touch = Input.GetTouch(i);
//			if (touch.phase == TouchPhase.Began && PinchZoomAwais.isPanning == false) {
//				
//				if (touch.position.x > (Screen.width*0.7f) && (touch.position.y < (Screen.width/1.3f))) {
//					Application.LoadLevel(3);
//					//do something
//				}
//			}
//		}
//		
//		#endif


	
	}

	void OnGUI(){

		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		GUI.depth = 1;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

		GUI.DrawTexture (BG1Pos, BG1Img);

		GUI.DrawTexture (text1Pos, text1Img);

		/*if (GUI.Button (touchButtonNextPOs, "", touchButtonStyle)) {
			Application.LoadLevel(1);
		}

		/*if (GUI.Button (nextPos, "", nextStyle)) {
			Application.LoadLevel(3);
		}*/
		//Menu Pannel
		if (!isMenu) {
			if (GUI.Button (menuIconPos, "", menuIconStyle)) {
				isMenu = true;
			
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = true;
				
			}
		}

		if(isMenu){

			GUI.DrawTexture(menuBGPos,menuBG);
			GUI.DrawTexture(homeTextRect,homeText);
			GUI.DrawTexture(restartTextRect,restartText);
			GUI.DrawTexture(settingTextRect,settingText);
			GUI.DrawTexture(SkipTextRect, SkipText);

			if (GUI.Button (crossPos, "", crossStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = false;
				
			}
			if (GUI.Button (SkipPos, "", SkipStyle)) {
				isMenu = false;
                Application.LoadLevel("PuppyP9");
            }
			if (GUI.Button (restartPos, "", restartStyle)) {
				isMenu = false;
				Application.LoadLevel("PuppyP8");
				
			}
			if (GUI.Button (settingPos, "", settingStyle)) {
				isMenu = false;
				Application.LoadLevel("SettingsMenu");
				
			}
			if (GUI.Button (homePos, "", homeStyle)) {
				isMenu = false;
				Application.LoadLevel("MAinMenuG");
				
			}

		}
		// end of menu Panel
	}
}

﻿using UnityEngine;
using System.Collections;

public class TidesP1 : MonoBehaviour {
	private float screenWidth = 1280f;
	private float screenHeight = 800f;

	public GUIStyle nextStyle;
	public Rect nextPos;

	//Menu Variables
	private bool isMenu = false;
	
	public Texture menuBG;
	public Rect menuBGPos;
	
	public GUIStyle menuIconStyle;
	public Rect menuIconPos;
	public GUIStyle menuIconStyle2;
	public Rect menuIconPos2;
	
	public GUIStyle bookMarkStyle;
	public Rect bookMarkPos;
	
	public GUIStyle restartStyle;
	public Rect restartPos;
	
	public GUIStyle settingStyle;
	public Rect settingPos;
	
	public GUIStyle homeStyle;
	public Rect homePos;
	
	public GUIStyle crossStyle;
	public Rect crossPos;
	//end off Menu variables
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

		if (GUI.Button (nextPos, "", nextStyle)) {
			Application.LoadLevel(12);
		}


		//Menu Pannel
		if (!isMenu) {
			if (GUI.Button (menuIconPos, "", menuIconStyle)) {
				isMenu = true;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = true;
				
			}
		}
		
		if(isMenu){
			
			GUI.DrawTexture(menuBGPos,menuBG);
			if (GUI.Button (crossPos, "", crossStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = false;
				
			}
			if (GUI.Button (bookMarkPos, "", bookMarkStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (restartPos, "", restartStyle)) {
				isMenu = false;
				Application.LoadLevel(Application.loadedLevel);
				
			}
			if (GUI.Button (settingPos, "", settingStyle)) {
				isMenu = false;
				Application.LoadLevel(1);
				
			}
			if (GUI.Button (homePos, "", homeStyle)) {
				isMenu = false;
				Application.LoadLevel(0);
				
			}
			
		}
		// end of menu Panel
	}
}

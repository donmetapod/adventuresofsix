﻿using UnityEngine;
using System.Collections;

public class GoldFishP40 : MonoBehaviour {

	
	/// <summary>
	/// DrawTextures All for Menu	/// </summary>
	
	public Texture homeText;
	public Texture restartText;
	public Texture settingText;
	public Texture bookMarkText;
	
	public Rect homeTextRect;
	public Rect restartTextRect;
	public Rect settingTextRect;
	public Rect bookMarkTextRect;
	
	
	/// <summary>
	/// T/////end	/// </summary>
	private float screenWidth = 2048f;
	private float screenHeight = 1536f;
	
	
	public Texture BG1Img;
	public Rect BG1Pos;
	
	
	
	public Texture text1Img;
	public Rect text1Pos;
	
	public Texture text2Img;
	public Rect text2Pos;

	public Texture text3Img;
	public Rect text3Pos;
	
	public GUIStyle nextStyle;
	public Rect nextPos;
	//Menu Variables
	private bool isMenu = false;
	
	public Texture menuBG;
	public Rect menuBGPos;
	
	public GUIStyle menuIconStyle;
	public Rect menuIconPos;
	public GUIStyle menuIconStyle2;
	public Rect menuIconPos2;
	
	public GUIStyle bookMarkStyle;
	public Rect bookMarkPos;
	
	public GUIStyle restartStyle;
	public Rect restartPos;
	
	public GUIStyle settingStyle;
	public Rect settingPos;
	
	public GUIStyle homeStyle;
	public Rect homePos;
	
	public GUIStyle crossStyle;
	public Rect crossPos;
	//end off Menu variables
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		
	}
	
	void OnGUI(){
		
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));
		
		GUI.DrawTexture (BG1Pos, BG1Img);
		
		GUI.DrawTexture (text1Pos, text1Img);
		GUI.DrawTexture (text2Pos, text2Img);
		GUI.DrawTexture (text3Pos, text3Img);
		
		if (GUI.Button (nextPos, "", nextStyle)) {
			Application.LoadLevel(7);
		}
		//Menu Pannel
		if (!isMenu) {
			if (GUI.Button (menuIconPos, "", menuIconStyle)) {
				isMenu = true;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = true;
				
			}
		}
		
		if(isMenu){
			GUI.DrawTexture(menuBGPos,menuBG);
			GUI.DrawTexture(homeTextRect,homeText);
			GUI.DrawTexture(restartTextRect,restartText);
			GUI.DrawTexture(settingTextRect,settingText);
			GUI.DrawTexture(bookMarkTextRect,bookMarkText);
			

			if (GUI.Button (crossPos, "", crossStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = false;
				
			}
			if (GUI.Button (bookMarkPos, "", bookMarkStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (restartPos, "", restartStyle)) {
				isMenu = false;
				Application.LoadLevel(2);
				
			}
			if (GUI.Button (settingPos, "", settingStyle)) {
				isMenu = false;
				Application.LoadLevel(1);
				
			}
			if (GUI.Button (homePos, "", homeStyle)) {
				isMenu = false;
				Application.LoadLevel(0);
				
			}
			
		}
		// end of menu Panel
	}
}

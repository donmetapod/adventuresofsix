﻿using UnityEngine;
using System.Collections;

public class TidesP2 : MonoBehaviour {
	private bool isQuestion = false;
	private bool isDone = false;

	public GameObject dialogue2Plane;
	public GameObject dialogue1Plane;

	public GameObject question1Plane;
	public GameObject plane2;
	public GameObject plane1;
	private float screenWidth = 1280f;
	private float screenHeight = 800f;
	//Menu Variables
	private bool isMenu = false;
	
	public Texture menuBG;
	public Rect menuBGPos;
	
	public GUIStyle menuIconStyle;
	public Rect menuIconPos;
	public GUIStyle menuIconStyle2;
	public Rect menuIconPos2;
	
	public GUIStyle bookMarkStyle;
	public Rect bookMarkPos;
	
	public GUIStyle restartStyle;
	public Rect restartPos;
	
	public GUIStyle settingStyle;
	public Rect settingPos;
	
	public GUIStyle homeStyle;
	public Rect homePos;
	
	public GUIStyle crossStyle;
	public Rect crossPos;
	// Use this for initialization

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonDown(0)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100)) {
				//			print ("Hit"+hit.transform.tag);
				if(hit.transform.tag == "CubeQuestion1"){
					isQuestion = true;
				}
				if(hit.transform.tag == "Done"){
					isQuestion = false;
					isDone = true;
					dialogue2Plane.SetActive(true);
					dialogue1Plane.SetActive(false);
				}
				if(hit.transform.tag == "CorrectOkBtn"){
					Application.LoadLevel(0);
				}

			}
		}

		if(isQuestion){
			question1Plane.SetActive(true);
			plane2.SetActive(true);
			plane1.SetActive(false);

		}
		if(!isQuestion){
			if(!isDone){
			question1Plane.SetActive(false);
			plane2.SetActive(false);
			plane1.SetActive(true);
			}
			else{
				question1Plane.SetActive(false);
				plane2.SetActive(true);
				plane1.SetActive(false);
			}

		}
	
	}

	void OnGUI(){
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

		//Menu Pannel
		if (!isMenu) {
			if (GUI.Button (menuIconPos, "", menuIconStyle)) {
				isMenu = true;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = true;
				
			}
		}
		
		if(isMenu){
			
			GUI.DrawTexture(menuBGPos,menuBG);
			if (GUI.Button (crossPos, "", crossStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = false;
				
			}
			if (GUI.Button (bookMarkPos, "", bookMarkStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (restartPos, "", restartStyle)) {
				isMenu = false;
				Application.LoadLevel(Application.loadedLevel);
				
			}
			if (GUI.Button (settingPos, "", settingStyle)) {
				isMenu = false;
				Application.LoadLevel(1);
				
			}
			if (GUI.Button (homePos, "", homeStyle)) {
				isMenu = false;
				Application.LoadLevel(0);
				
			}
			
		}
		// end of menu Panel
	}
}

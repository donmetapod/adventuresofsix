﻿using UnityEngine;
using System.Collections;

public class GoldFishP10 : MonoBehaviour {

	
	/// <summary>
	/// DrawTextures All for Menu	/// </summary>
	
	public Texture homeText;
	public Texture restartText;
	public Texture settingText;
	public Texture bookMarkText;
	
	public Rect homeTextRect;
	public Rect restartTextRect;
	public Rect settingTextRect;
	public Rect bookMarkTextRect;
	
	
	/// <summary>
	/// T/////end	/// </summary>
	private bool ansTrue = false;
	public GameObject question1Plane;
	public GameObject question2Plane;
	private float screenWidth = 2048f;
	private float screenHeight = 1536f;
	private bool isQuestion = false;
	private bool next1 = false;
	private int count = 0;

	public Texture BG1;
	public Rect BG1Pos;

	public Texture BG2;
	public Rect BG2Pos;

	public Texture text1;
	public Rect text1Pos;

	public GUIStyle btn1Style;
	public Rect btn1Pos;

	public Texture dialogue1;
	public Rect dialoguePos;

    public GUIStyle next1Style;
    public Rect next1Pos;

    public Texture dialogue2;
	public Rect dialogue2Pos;


	//Menu Variables
	private bool isMenu = false;

	public Texture menuBG;
	public Rect menuBGPos;

	public GUIStyle menuIconStyle;
	public Rect menuIconPos;
	public GUIStyle menuIconStyle2;
	public Rect menuIconPos2;
	
	public GUIStyle bookMarkStyle;
	public Rect bookMarkPos;
	
	public GUIStyle restartStyle;
	public Rect restartPos;
	
	public GUIStyle settingStyle;
	public Rect settingPos;
	
	public GUIStyle homeStyle;
	public Rect homePos;
	
	public GUIStyle crossStyle;
	public Rect crossPos;
	//end off Menu variables

	//Dialogue

	public GUISkin DialogueSkin;
	public Rect pencilPos;
	public Rect pencilTickPos;
	public Rect gamesPos;
	public Rect gamesTickPos;
	public Rect paperPos;
	public Rect paperTickPos;
	public Rect pensPos;
	public Rect pensTickPos;
	public Rect lunchPos;
	public Rect lunchTickPos;
	public Rect bookPos;
	public Rect bookTickPos;
	public Rect homeWorkPos;
	public Rect homeWorkTickPos;
	public Rect snacksPos;
	public Rect snacksTickPos;

	private bool pencil = false;
	private bool games = false;
	private bool paper = false;
	private bool pens = false;
	private bool lunch = false;
	private bool book = false;
	private bool homeWork = false;
	private bool snack = false;

	private bool wrongAns = false;

	public Texture wrongDialogue;
	public Rect wrongDiaPos;

	public GUIStyle btnOkStyle;
	public GUIStyle btnOkStyle2;
	public Rect btnOkPos2;
	public Rect btnOkPos;

	void OnTriggerEnter(Collider other) {

	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 100)) {
			print ("Hit"+hit.transform.tag);
			if(hit.transform.tag == "CubeQuestion1"){
			isQuestion = true;
			}
		}
		}
	
	}

	void OnGUI(){
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

		if (!isQuestion) {
			question1Plane.SetActive(true);

//			if (GUI.Button (btn1Pos, "", btn1Style)) {
//				isQuestion = true;
//		
//			}
		}

		if (isQuestion) {
			question1Plane.SetActive(false);
			question2Plane.SetActive(true);
			//GUI.DrawTexture (BG2Pos, BG2);
			if(!wrongAns){
			GUI.DrawTexture (dialoguePos, dialogue1);
				if(!ansTrue){
			if (!pencil) {
				if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
					pencil = true;
					print ("pencil true");
				}
			} else {
				if (GUI.Button (pencilTickPos, "", DialogueSkin.customStyles [1])) {
					pencil = false;
				}
			}

			if (!games) {
				if (GUI.Button (gamesPos, "", DialogueSkin.customStyles [2])) {
					games = true;
					print ("games true");
				}
			} else {
				if (GUI.Button (gamesTickPos, "", DialogueSkin.customStyles [3])) {
					games = false;
				}
			}

			if (!paper) {
				if (GUI.Button (paperPos, "", DialogueSkin.customStyles [4])) {
					paper = true;
					print ("paper true");
				}
			} else {
				if (GUI.Button (paperTickPos, "", DialogueSkin.customStyles [5])) {
					paper = false;
				}
			}

			if (!pens) {
				if (GUI.Button (pensPos, "", DialogueSkin.customStyles [6])) {
					pens = true;
					print ("pens true");
				}
			} else {
				if (GUI.Button (pensTickPos, "", DialogueSkin.customStyles [7])) {
					pens = false;
				}
			}
				

			if (!lunch) {
				if (GUI.Button (lunchPos, "", DialogueSkin.customStyles [8])) {
					lunch = true;
					print ("lunch true");
				}
			} else {
				if (GUI.Button (lunchTickPos, "", DialogueSkin.customStyles [9])) {
					lunch = false;
				}
			}

			if (!book) {
				if (GUI.Button (bookPos, "", DialogueSkin.customStyles [10])) {
					book = true;
					print ("book true");
				}
			} else {
				if (GUI.Button (bookTickPos, "", DialogueSkin.customStyles [11])) {
					book = false;
				}
			}

			if (!homeWork) {
				if (GUI.Button (homeWorkPos, "", DialogueSkin.customStyles [12])) {
					homeWork = true;
					print ("homework true");
				}
			} else {
				if (GUI.Button (homeWorkTickPos, "", DialogueSkin.customStyles [13])) {
					homeWork = false;
				}
			}

			if (!snack) {
				if (GUI.Button (snacksPos, "", DialogueSkin.customStyles [14])) {
					snack = true;
					print ("snack true");
				}
			} else {
				if (GUI.Button (snacksTickPos, "", DialogueSkin.customStyles [15])) {
					snack = false;
				}
			}
				}


			if (GUI.Button (next1Pos, "", next1Style)) {
				if (pencil && paper && pens && book && homeWork && !games && !lunch && !snack) {
					next1 = true;
						count++;
				} else {
					wrongAns = true;

				}
				//count++;
				print ("MyCount" + count);
				if (count == 2) {
					Application.LoadLevel (4);
				}
			}
		}

			if(wrongAns){
				GUI.DrawTexture(wrongDiaPos,wrongDialogue);
				if(GUI.Button(btnOkPos2,"",btnOkStyle)){
					//wrongAns = false;
				}
				if(GUI.Button(btnOkPos,"OK",btnOkStyle2)){
					wrongAns = false;
				}

			}

		}

		if (next1) {
			GUI.DrawTexture(dialogue2Pos,dialogue2);
			ansTrue = true;
		
		}

		//Menu Pannel
		if (!isMenu) {
			if (GUI.Button (menuIconPos, "", menuIconStyle)) {
				isMenu = true;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = true;
				
			}
		}
		
		if(isMenu){
			GUI.DrawTexture(menuBGPos,menuBG);

			GUI.DrawTexture(homeTextRect,homeText);
			GUI.DrawTexture(restartTextRect,restartText);
			GUI.DrawTexture(settingTextRect,settingText);
			GUI.DrawTexture(bookMarkTextRect,bookMarkText);


			if (GUI.Button (crossPos, "", crossStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = false;
				
			}
			if (GUI.Button (bookMarkPos, "", bookMarkStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (restartPos, "", restartStyle)) {
				isMenu = false;
				Application.LoadLevel(2);
				
			}
			if (GUI.Button (settingPos, "", settingStyle)) {
				isMenu = false;
				Application.LoadLevel(1);
				
			}
			if (GUI.Button (homePos, "", homeStyle)) {
				isMenu = false;
				Application.LoadLevel(0);
				
			}
			
		}
		// end of menu Panel

	
	}
}

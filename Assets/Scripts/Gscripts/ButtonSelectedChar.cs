﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSelectedChar : MonoBehaviour {
	public int value;
	bool isSelected = false;

		// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (value == 1) {
            isSelected = AdventureSettings.isMale;
        } else if (value == 2) {
            isSelected = !AdventureSettings.isMale;
        } else if (value == 3) {
            isSelected = AdventureSettings.isFemale;
        } else if (value == 4) {
            isSelected = !AdventureSettings.isFemale;
        } 

		if (isSelected){
			GetComponent<Image>().color = new Color(PlayerPrefs.GetFloat("r"), PlayerPrefs.GetFloat("g"), PlayerPrefs.GetFloat("b"));
			if (transform.name == "GirlButton")
			{
				GameObject.Find("GirlBackground").GetComponent<Image>().color = Color.white;
			}
			else
			{
				GameObject.Find("BoyBackground").GetComponent<Image>().color = Color.white;
			}
		} else{
			GetComponent<Image>().color = Color.gray;
			//GetComponent<Image>().color = new Color(PlayerPrefs.GetFloat("r"), PlayerPrefs.GetFloat("g"), PlayerPrefs.GetFloat("b"));
			if (transform.name == "GirlButton")
			{
				GameObject.Find("GirlBackground").GetComponent<Image>().color = Color.gray;
			}
			else
			{
				GameObject.Find("BoyBackground").GetComponent<Image>().color = Color.gray;
			}

		}

	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogTidiesup1 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = " After the fun time, comes the tidy up time. The kids looked around the room and realized it was a big mess. Blankets, sheets and feathers were everywhere! Fiona and " + charName + " both agreed that neither of them wanted to tidy up the feathers. After a quick game of rock, paper, scissors, which Fiona lost, she was off to get the vacuum and Angus began to put away the fortresses.";

    }
}

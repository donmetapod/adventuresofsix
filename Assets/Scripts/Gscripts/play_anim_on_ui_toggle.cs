﻿//Script Made By StrupsGames//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class play_anim_on_ui_toggle : MonoBehaviour 
{

	public Toggle Text;
	public AudioClip sound;
	public Animator animationOne;
	public Animator animationTwo;
	public Animator animationThree;
	public Canvas yourcanvas;
	public int animationID;


	void Start () 
	{
		AudioListener.volume = 1.0f;
		Text = Text.GetComponent<Toggle> ();
		animationOne.enabled = false;
		animationTwo.enabled = false;
		animationThree.enabled = false;
		yourcanvas.enabled = true;
	}

	public void Press() 
	{
		animationID = Random.Range (0, 3);
		AudioSource.PlayClipAtPoint(sound, transform.position);
		Debug.Log(animationOne);
		if (animationID == 0) {
			animationOne.enabled = true;
			animationOne.Play("Game_Homework01", -1, 0f);
		} else if (animationID == 1) {
			animationTwo.enabled = true;
			animationTwo.Play("Game_Homework02", -1, 0f);
		} else {
			animationThree.enabled = true;
			animationThree.Play("Game_Homework03", -1, 0f);
		}
		Text.enabled = true;
	}
}
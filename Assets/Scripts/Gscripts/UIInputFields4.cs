﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIInputFields4 : MonoBehaviour {
	
	public Text charText;
	public Text placeholderText;

	// Use this for initialization
	void Start () {
	
		charText = GameObject.Find ("CharText").GetComponent<Text> ();
		placeholderText = GameObject.Find("Placeholder").GetComponent<Text>(); 
	}
	
	public void CharacterField (string inputFieldString)
	{
		charText.text = inputFieldString;
	}

	public void ConfirmName()
	{
		if (charText.text.Length >= 3) {
			Debug.Log ("Confirmed Player Name");
			PlayerPrefs.SetString ("charName", charText.text);
			Application.LoadLevel ("SpainMainSettings02");
		}
		else
			Debug.Log ("Please Enter a Name of Increse Name Size");
		
		//if (placeholderText.enabled == true)
		//	Debug.Log (" Please enter a Name");
		//else
		//	Debug.Log (" Confirmed Player Name");
	}
}



////GoldFishOpening0
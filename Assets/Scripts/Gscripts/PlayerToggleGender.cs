﻿using UnityEngine;
using System.Collections;

public class PlayerToggleGender : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	public void setFemale() {
		AdventureSettings.isMale = false;
	}

	public void setMale() {
		AdventureSettings.isMale = true;
	}
}

﻿using UnityEngine;
using System.Collections;

public class ClickSound_Toggle : MonoBehaviour {

	public Animator LEDlights;


	public void StopLEDLightsAnimation()
	{
		LEDlights.SetBool("isPlaying", true);

	}

	public void PlayLEDLightsAnimation()
	{
		LEDlights.SetBool("isPlaying", !LEDlights.GetBool("isPlaying") );
	}
}
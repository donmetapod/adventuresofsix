﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialog : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = "The humans are Fiona who is seven, " + charName + " who is four and their mother. The two animals that also live in the house are a cat with six toes on each foot called Six and a tiny mouse called Squeaky.";
	}
}
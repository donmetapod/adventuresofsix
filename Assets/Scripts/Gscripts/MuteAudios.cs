﻿using UnityEngine;
using System.Collections;

public class MuteAudios : MonoBehaviour {
	
	void Start() {
	}

	public void Mutemusic() {
		AdventureSettings.playmusic = false;
	}

	public void playMusic(){
		AdventureSettings.playmusic = true;
	}

	public void MutenNaration ()
	{
		AdventureSettings.playnarration = false;
	}

	public void playNarration (){
		AdventureSettings.playnarration = true;
	}
}
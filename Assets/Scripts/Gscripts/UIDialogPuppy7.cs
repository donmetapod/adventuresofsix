﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogPuppy7 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText1").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = " " + charName + " and Fiona began to argue. Six watched them go back and forth. All of a sudden, the puppy was in between them. The bickering stopped, and the love for their puppy poured out of their hearts as they both smiled and gently stroked him. ";

    }
}

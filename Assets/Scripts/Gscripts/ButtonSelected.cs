﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSelected : MonoBehaviour {
	public int value;
	bool isSelected = false;

		// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (value == 1) {
			isSelected = AdventureSettings.playmusic;
		} else if (value == 2) {
			isSelected = !AdventureSettings.playmusic;
		} else if (value == 3) {
			isSelected = AdventureSettings.playnarration;
		} else if (value == 4) {
			isSelected = !AdventureSettings.playnarration;
		}

		if (isSelected){
			GetComponent<Image>().color = new Color(0, 233, 233);
		} else{
			GetComponent<Image>().color = Color.white;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeatherControl : MonoBehaviour {

	//Setting panel

	public GUIStyle dayTickStyle;
	public Rect dayTickPos;

	public GUIStyle dayStyle;
	public Rect dayPos;

	public GUIStyle rainStyle;
	public Rect rainPos;

	public GUIStyle rainTickStyle;
	public Rect rainTickPos;

	public GUIStyle snowTickStyle;
	public Rect snowTickPos;

	public GUIStyle snowStyle;
	public Rect snowPos;

	public GUIStyle sunnyStyle;
	public Rect sunnyPos;

	public GUIStyle sunnyTickStyle;
	public Rect sunnyTickPos;

	public GUIStyle nightStyle;
	public Rect nightPos;

	public GUIStyle nightTickStyle;
	public Rect nightTickPos;

	// Use this for initialization
	void Start () {

		PlayerPrefs.SetString ("Weather", "Day");
		PlayerPrefs.SetString ("DayMode", "Sunny");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void weather (){
		
	if(PlayerPrefs.GetString("Weather") == "Day"){
		if(GUI.Button(dayTickPos,"",dayTickStyle)){
			PlayerPrefs.SetString("Weather","Day");
		}
		if(GUI.Button(rainPos,"",rainStyle)){
			PlayerPrefs.SetString("Weather","Rain");
		}
		if(GUI.Button(snowPos,"",snowStyle)){
			PlayerPrefs.SetString("Weather","Snow");
		}

	}

	if(PlayerPrefs.GetString("Weather") == "Rain"){
		if(GUI.Button(dayPos,"",dayStyle)){
			PlayerPrefs.SetString("Weather","Day");
		}
		if(GUI.Button(rainTickPos,"",rainTickStyle)){
			PlayerPrefs.SetString("Weather","Rain");
		}
		if(GUI.Button(snowPos,"",snowStyle)){
			PlayerPrefs.SetString("Weather","Snow");
		}

	}

	if(PlayerPrefs.GetString("Weather") == "Snow"){
		if(GUI.Button(dayPos,"",dayStyle)){
			PlayerPrefs.SetString("Weather","Day");
		}
		if(GUI.Button(rainPos,"",rainStyle)){
			PlayerPrefs.SetString("Weather","Rain");
		}
		if(GUI.Button(snowTickPos,"",snowTickStyle)){
			PlayerPrefs.SetString("Weather","Snow");
		}

	}

	if(PlayerPrefs.GetString("DayMode") == "Sunny"){

		if(GUI.Button(sunnyTickPos,"",sunnyTickStyle)){
			PlayerPrefs.SetString("DayMode","Sunny");
		}
		if(GUI.Button(nightPos,"",nightStyle)){
			PlayerPrefs.SetString("DayMode","Night");
		}

	}

	if(PlayerPrefs.GetString("DayMode") == "Night"){

		if(GUI.Button(sunnyPos,"",sunnyStyle)){
			PlayerPrefs.SetString("DayMode","Sunny");
		}
		if(GUI.Button(nightTickPos,"",nightTickStyle)){
			PlayerPrefs.SetString("DayMode","Night");
		}

	}

	Character character = GetComponent<Character>();			

}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ButtonNextLevel : MonoBehaviour 
{
	public void NextLevelButton(int index)
	{
		Application.LoadLevel(index);
		Debug.Log (AdventureSettings.name);
	}

	public void NextLevelButton(string levelName)
	{
		if (levelName == "Custom")
		{
			Application.LoadLevel(PlayerPrefs.GetString("PathFollowed"));
		}
		else
		{
			Application.LoadLevel(levelName);	
		}
		
		//Debug.Log (AdventureSettings.name);
	}
	
//	public void NextLevelButtonWithCustomNarration(string levelName)
//	{
//		PlayerPrefs.SetInt("AltNarration", 1);
//		if (levelName == "Custom")
//		{
//			Application.LoadLevel(PlayerPrefs.GetString("PathFollowed"));
//		}
//		else
//		{
//			Application.LoadLevel(levelName);	
//		}
//	}
//
//	public void NextLevelButtonWithNoNarration(string levelName)
//	{
//		PlayerPrefs.SetInt("AltNarration", 2);
//		if (levelName == "Custom")
//		{
//			Application.LoadLevel(PlayerPrefs.GetString("PathFollowed"));
//		}
//		else
//		{
//			Application.LoadLevel(levelName);	
//		}
//	}

	public void LoadNextLevelReadingToggle()
	{
		//ToggleGroup toggleGroup = GameObject.Find("NarrationTypeSelect").GetComponent<ToggleGroup>();
		//IEnumerable<Toggle> togglesInGroup = toggleGroup.ActiveToggles();
		Toggle originalNarration = GameObject.Find("ToggleON").GetComponent<Toggle>();
		Toggle customNarration = GameObject.Find("ToggleCN").GetComponent<Toggle>();
		Toggle noNarration = GameObject.Find("ToggleNO").GetComponent<Toggle>();

		if (noNarration.isOn) {
			PlayerPrefs.SetInt("AltNarration", 2);
		}else if(customNarration.isOn){
			PlayerPrefs.SetInt("AltNarration", 1);
		}else{
			PlayerPrefs.SetInt("AltNarration", 0);	
		}

		Application.LoadLevel(PlayerPrefs.GetString("PathFollowed"));
	}

	public void SavePathAndLoad(string pathToFollow)
	{
		PlayerPrefs.SetString("PathFollowed", pathToFollow);
		NextLevelButton("MainSettings01");
	}
	
}
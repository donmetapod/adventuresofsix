﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class LoadLevelTimer_00 : MonoBehaviour {
    public string LevelToLoad;
	public float timer = 3f;
    private Text timeSeconds;


	// Use this for initialization
	void Start ()
	{
		print(transform.name);
        timeSeconds = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

		timer -= Time.deltaTime;
        timeSeconds.text = timer.ToString("f0");
        if (timer <= 0)
        {
            Application.LoadLevel(LevelToLoad);
        }
	
	}
}

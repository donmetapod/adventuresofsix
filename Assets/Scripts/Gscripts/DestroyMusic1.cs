﻿using UnityEngine;
using System.Collections;

public class DestroyMusic1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject Music = GameObject.Find ("Music1");
		if (Music) {
			Destroy(Music);
		}
		DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		AudioSource audio = GetComponent<AudioSource> ();
		if (AdventureSettings.playmusic) {
			if (!audio.isPlaying)
				audio.Play ();
		} else {
			audio.Stop ();
		}
	}
}

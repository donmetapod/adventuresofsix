﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogPuppy : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = "Six had heard the discussions now and again for the past few months mostly during mealtimes, about a puppy that would be joining the family. “Who would need more than me?” Six thought. “He’s here!” shouted " + charName + " as he and Fiona raced down the stairs to the front door. “Can you see him?” said " + charName + " trying to tiptoe up and around his sister. “No, but I see a travel crate, he must be inside,” said Fiona. ";
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogTidiesup3 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = " Now Six was sleepier than ever. She walked slowly back to her favorite tree. Just then, Six heard the school bus arrive, which means " + charName + " and Fiona are home. The kids ran through the yard and began playing in the tidy pile of scrunchy leaves.";

    }
}

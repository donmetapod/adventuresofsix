﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogPuppy4 : MonoBehaviour 
{

    public Text dialogText;

    string charName;

    string dialogString;

    // Use this for initialization
    void Start()
    {
        dialogText = GameObject.Find("MainText").GetComponent<Text>();

        charName = PlayerPrefs.GetString("charName");
    }

    // Update is called once per frame
    void Update()
    {
        dialogText.text = dialogString;

        dialogString = " “Well, not too far, just to Number 30 and back,” said Mom. “Okay!” they yelled, as " + charName + " grabbed the red leash and attached it to Gulliver’s red collar leading him out of the front door.";

    }
}

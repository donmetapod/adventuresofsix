﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogShopping2 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = " Mrs. Donaldson apologized to the store clerk and quickly left the grocery store. She promised Six to get her home soon and tried to figure out how Six got into the shopping bag. Six had climbed to the top of the bag and poked her head out, looking left, and then to the right. It was then that she saw Fiona and  " + charName + " with their mother, leaving the shoe shop. The kids had happy, smiling faces. Six thought, “Mom could use a cat nap.” ";

    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogPuppy2 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText02").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = "It was then that they noticed he had a red collar around his neck with a gold, circular tag attached. Fiona turned the tag over. “Gulliver,” said " + charName + ". ";
	}
}

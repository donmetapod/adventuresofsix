﻿using UnityEngine;
using System.Collections;

public class mute : MonoBehaviour {

	bool isMute;

	public void Mute(){
		isMute = !isMute;
		AudioListener.volume =  isMute ? 0 : 1;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

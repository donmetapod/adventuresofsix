﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogPuppy3 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = "“You’re going to love my new shoes, Six” " + charName + " said. He then opened the door on the other side saying, “Thanks for taking us to the movie, Mom. I never would have guessed that the horse and snake would have become friends.” Mom replied, “Poor Six, she must be starving.” “Poor kitty, extra big plate tonight,” said Fiona. “Meow,” said Six, as they all went into the house. “Night, night.” ";
	}
}

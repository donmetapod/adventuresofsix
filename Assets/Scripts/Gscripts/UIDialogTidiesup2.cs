﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogTidiesup2 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = " After playing in the yard, " + charName + " and Fiona both enter the house to get ready for dinner. The piles of leaves are now all jumbled up again. Six has all but given up making the neat piles of leaves and starts walking slowly into the house.";

    }
}

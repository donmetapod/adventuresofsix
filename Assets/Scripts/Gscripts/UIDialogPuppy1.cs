﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogPuppy1 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText2").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = "Fiona and " + charName + " spoke to the puppy and told him all about themselves and the neighborhood. They then went through the list of names to see which one they thought he would like. Soon, the list of names was down to two:  Jeeves and Walter ";
	}
}

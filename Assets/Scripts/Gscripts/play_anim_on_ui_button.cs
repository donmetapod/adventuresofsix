﻿//Script Made By StrupsGames//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class play_anim_on_ui_button : MonoBehaviour 
{

	public Button Text;
	public AudioClip sound;
	public Animator animationOne;
	public Animator animationTwo;
	public Animator animationThree;
	//public Canvas yourcanvas;
	public int animationID;


	void Start () 
	{
		Text = Text.GetComponent<Button> ();
		animationOne.enabled = false;
		animationTwo.enabled = false;
		animationThree.enabled = false;
		//yourcanvas.enabled = true;
	}

	public void Press() 
	{
		animationID = Random.Range (0, 3);
		AudioSource.PlayClipAtPoint(sound, transform.position);
		if (animationID == 0) {
			Debug.Log(animationOne);
			animationOne.enabled = true;
			animationOne.Play("Six_in&out01", -1, 0f);
		} else if (animationID == 1) {
			Debug.Log(animationID);
			animationTwo.enabled = true;
			animationTwo.Play("Six_in&out01", -1, 0f);
		} else {
			Debug.Log(animationID);
			animationThree.enabled = true;
			animationThree.Play("Six_in&out01", -1, 0f);
		}
		StartCoroutine(HideAndShowColorPicker());
		Text.enabled = true;
	}

	IEnumerator HideAndShowColorPicker()
	{
		GameObject colorPicker = GameObject.Find("ColorPallet");
		if (colorPicker == null)
		{
			yield return null;
		}
		else
		{
			colorPicker.SetActive(false);
			yield return new WaitForSeconds(3);
			colorPicker.SetActive(true);
		}
		
	}
}
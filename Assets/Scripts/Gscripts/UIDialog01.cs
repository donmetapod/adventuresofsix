﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialog01 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = "After another fun weekend it was Monday morning and Six watched as Fiona and " + charName + " got their school bags and checked to make sure they had their homework, books, paper, pens and pencils. “Don’t forget your lunch and snacks,” said Mom. The front door closed with a bang and Six could hear the car start and then leave.";
	}
}

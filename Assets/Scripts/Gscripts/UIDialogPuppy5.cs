﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogPuppy5 : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText01").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = " The puppy began jumping up and down onto their laps. From " + charName + " to Fiona, back to " + charName + " when his name was called. The kids were laughing as they called Gulliver back and forth. ";

    }
}

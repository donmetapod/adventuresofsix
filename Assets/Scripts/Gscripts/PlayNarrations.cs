﻿using UnityEngine;
using System.Collections;

public class PlayNarrations : MonoBehaviour {
	private bool played;
	AudioSource audio;

	// Use this for initialization
	void Start () {

		audio = GetComponent<AudioSource> ();
		if (PlayerPrefs.GetInt("AltNarration") == 2) //no audio
		{
			audio.volume = 0.0f;
		}
		else
		{
			audio.volume = 0.7f;
		}

		GameObject audioM = GameObject.Find ("AudioManager");
		if (audioM != null) {
			audioM.GetComponent<AudioSource>().volume = 0.3f;
		}
		played = false;
		string currentSceneName = GameObject.Find ("SceneName").GetComponent<SceneName> ().currentSceneName;
		if (PlayerPrefs.GetInt ("AltNarration") == 1) {
			string path = Application.persistentDataPath + "/" + currentSceneName + ".wav";
			WWW www = new WWW ("file:///" + path);
			AudioClip customClip = WWWAudioExtensions.GetAudioClip (www, false, false, AudioType.WAV);
			audio.clip = customClip;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (AdventureSettings.playnarration) {
			if (!audio.isPlaying && !played) {
				audio.Play ();
				played = true;
			} else if (!audio.isPlaying && played) {
				audio.Stop ();
			}
		} else {
			audio.Stop ();
		}
	}
}

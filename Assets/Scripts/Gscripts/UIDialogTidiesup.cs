﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDialogTidiesup : MonoBehaviour 
{

	public Text dialogText;

	string charName;

	string dialogString;

	// Use this for initialization
	void Start () 
	{
		dialogText = GameObject.Find ("MainText").GetComponent<Text> ();

		charName = PlayerPrefs.GetString ("charName");
	}
	
	// Update is called once per frame
	void Update () 
	{
		dialogText.text = dialogString;

		dialogString = " Inside the house, Fiona and " + charName + " were playing pillow wars in the living room. They had built fortresses with sheets and blankets. Fiona and " + charName + " were enjoying the back and forth of pillow ﬁghting. As usual, the screams and giggles got louder and louder. Fiona was taller than her brother so could keep him away with her reach when she needed to take a rest… and off they went again.";

    }
}

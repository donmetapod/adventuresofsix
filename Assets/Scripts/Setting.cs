﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Setting : MonoBehaviour {

	public GameObject canvaOff;
	public GameObject plane1;
	public GameObject plane2;
	public GameObject plane3;

	public Texture fishTitle;
	public Texture selectCharacter;


	public GUIStyle nextBtnStyle;
	public Rect nextBtn;

	private float screenWidth = 2048f;
	private float screenHeight = 1536f;


	public GUIStyle boyStyle;
	public GUIStyle boyStyle2;
	public Rect boyButton;


	public GUIStyle girlStyle;
	public GUIStyle girlStyle2;
	public Rect girlButton;
	//public GUIStyle selectCharacterStyle;
	public Rect selectCharacterPos;
	public Rect fishTitlePos;

	public GUIStyle inputTextStyle;
	public Rect inputTextPos;

	private bool next1 = false;
	private bool next2 = false;


	//Setting panel

	public GUIStyle nextBtn2Style;
	public Rect nextBtn2;

	public Texture settingImg;
	public Rect settingPos;

	public Texture BG2;
	public Rect BG2Pos;

	public GUIStyle dayTickStyle;
	public Rect dayTickPos;

	public GUIStyle dayStyle;
	public Rect dayPos;

	public GUIStyle rainStyle;
	public Rect rainPos;

	public GUIStyle rainTickStyle;
	public Rect rainTickPos;

	public GUIStyle snowTickStyle;
	public Rect snowTickPos;

	public GUIStyle snowStyle;
	public Rect snowPos;

	public GUIStyle sunnyStyle;
	public Rect sunnyPos;

	public GUIStyle sunnyTickStyle;
	public Rect sunnyTickPos;

	public GUIStyle nightStyle;
	public Rect nightPos;

	public GUIStyle nightTickStyle;
	public Rect nightTickPos;


	//Sound Panel
	public GUIStyle nextBtn3Style;
	public Rect nextBtn3;
	
	public Texture soundSettingImg;
	public Rect soundSettingPos;

	public GUIStyle soundOnStyle;
	public Rect soundOnPos;

	public GUIStyle soundOnTickStyle;
	public Rect soundOnTickPos;

	public GUIStyle soundOffStyle;
	public Rect soundOffPos;

	public GUIStyle soundOffTickStyle;
	public Rect soundOffTickPos;

	public GUIStyle narrationOnStyle;
	public Rect narrationOnPos;

	public GUIStyle narrationTickOnStyle;
	public Rect narrationTickOnPos;

	public GUIStyle narrationOffStyle;
	public Rect narrationOffPos;

	public GUIStyle narrationTickOffStyle;
	public Rect narrationTickOffPos;


	//public InputField myField;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetString ("Character", "Boy");
		PlayerPrefs.SetString ("Weather", "Day");
		PlayerPrefs.SetString ("DayMode", "Sunny");
		PlayerPrefs.SetString ("Sound", "On");
		PlayerPrefs.SetString ("Narration", "On");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

	//	inputTextStyle.wordWrap = false;
//		inputTextStyle.clipping = TextClipping.clip;
		GUI.DrawTexture (fishTitlePos,fishTitle);


		if (next1) {
			plane1.SetActive(false);
			plane2.SetActive(true);
			canvaOff.SetActive(false);
			//GUI.DrawTexture(BG2Pos,BG2);
			GUI.DrawTexture (fishTitlePos,fishTitle);
			GUI.DrawTexture(settingPos,settingImg);

			if (GUI.Button (nextBtn2, "", nextBtn2Style)) {
				next2 = true;
				next1 = false;
				
			}

			if(PlayerPrefs.GetString("Weather") == "Day"){
				if(GUI.Button(dayTickPos,"",dayTickStyle)){
					PlayerPrefs.SetString("Weather","Day");
				}
				if(GUI.Button(rainPos,"",rainStyle)){
					PlayerPrefs.SetString("Weather","Rain");
				}
				if(GUI.Button(snowPos,"",snowStyle)){
					PlayerPrefs.SetString("Weather","Snow");
				}

			}

			if(PlayerPrefs.GetString("Weather") == "Rain"){
				if(GUI.Button(dayPos,"",dayStyle)){
					PlayerPrefs.SetString("Weather","Day");
				}
				if(GUI.Button(rainTickPos,"",rainTickStyle)){
					PlayerPrefs.SetString("Weather","Rain");
				}
				if(GUI.Button(snowPos,"",snowStyle)){
					PlayerPrefs.SetString("Weather","Snow");
				}
				
			}

			if(PlayerPrefs.GetString("Weather") == "Snow"){
				if(GUI.Button(dayPos,"",dayStyle)){
					PlayerPrefs.SetString("Weather","Day");
				}
				if(GUI.Button(rainPos,"",rainStyle)){
					PlayerPrefs.SetString("Weather","Rain");
				}
				if(GUI.Button(snowTickPos,"",snowTickStyle)){
					PlayerPrefs.SetString("Weather","Snow");
				}
				
			}

			if(PlayerPrefs.GetString("DayMode") == "Sunny"){

				if(GUI.Button(sunnyTickPos,"",sunnyTickStyle)){
					PlayerPrefs.SetString("DayMode","Sunny");
				}
				if(GUI.Button(nightPos,"",nightStyle)){
					PlayerPrefs.SetString("DayMode","Night");
				}

			}

			if(PlayerPrefs.GetString("DayMode") == "Night"){
				
				if(GUI.Button(sunnyPos,"",sunnyStyle)){
					PlayerPrefs.SetString("DayMode","Sunny");
				}
				if(GUI.Button(nightTickPos,"",nightTickStyle)){
					PlayerPrefs.SetString("DayMode","Night");
				}
				
			}

            Character character = GetComponent<Character>();			
			
		}// end of next1


		if (next2) {

			plane3.SetActive(true);
			plane2.SetActive(false);
			plane1.SetActive(false);

			GUI.DrawTexture(soundSettingPos,soundSettingImg);
			if (GUI.Button (nextBtn3, "", nextBtn3Style)) {
				Application.LoadLevel("GoldFishOpening0");
				
			}
			if(PlayerPrefs.GetString("Sound")== "On"){
				if(GUI.Button(soundOnTickPos,"",soundOnTickStyle)){
					PlayerPrefs.SetString("Sound","On");
				}
				if(GUI.Button(soundOffPos,"",soundOffStyle)){
					PlayerPrefs.SetString("Sound","Off");
				}
			}

			if(PlayerPrefs.GetString("Sound")== "Off"){
				if(GUI.Button(soundOnPos,"",soundOnStyle)){
					PlayerPrefs.SetString("Sound","On");
				}
				if(GUI.Button(soundOffTickPos,"",soundOffTickStyle)){
					PlayerPrefs.SetString("Sound","Off");
				}
			}

			if(PlayerPrefs.GetString("Narration")== "On"){
				if(GUI.Button(narrationTickOnPos,"",narrationTickOnStyle)){
					PlayerPrefs.SetString("Narration","On");
				}
				if(GUI.Button(narrationOffPos,"",narrationOffStyle)){
					PlayerPrefs.SetString("Narration","Off");
				}
			}
			if(PlayerPrefs.GetString("Narration")== "Off"){
				if(GUI.Button(narrationOnPos,"",narrationOnStyle)){
					PlayerPrefs.SetString("Narration","On");
				}
				if(GUI.Button(narrationTickOffPos,"",narrationTickOffStyle)){
					PlayerPrefs.SetString("Narration","Off");
				}
			}

		}//end of next2

		if (!next1 && !next2) {
			plane1.SetActive(true);
			plane2.SetActive(false);
			GUI.DrawTexture (selectCharacterPos, selectCharacter);

			if (GUI.Button (nextBtn, "", nextBtnStyle)) {
				next1 = true;
				
			}



			if (PlayerPrefs.GetString ("Character") == "Boy") {
				if (GUI.Button (boyButton, "", boyStyle)) {
					//GUI.Toggle(new Rect(10, 10, 100, 30),true, "A Toggle text");
					//Application.LoadLevel(1);
				}
				if (GUI.Button (girlButton, "", girlStyle)) {
					PlayerPrefs.SetString ("Character", "Girl");
					//Application.LoadLevel(1);
				}
			}
			if (PlayerPrefs.GetString ("Character") == "Girl") {
				if (GUI.Button (boyButton, "", boyStyle2)) {
					PlayerPrefs.SetString ("Character", "Boy");
					//GUI.Toggle(new Rect(10, 10, 100, 30),true, "A Toggle text");
					//Application.LoadLevel(1);
				}
				if (GUI.Button (girlButton, "", girlStyle2)) {

					//Application.LoadLevel(1);
				}
			}

		}// end of next



		//GUI.TextField (inputTextPos,"");
		//GUI.TextArea (inputTextPos, "Here");
	}
}

﻿using UnityEngine;
using System.Collections;

public class PuppyP2Q2Handler : MonoBehaviour {
	public int count = 0;
	public GameObject numberCube;
	public Material[] myMaterials = new Material[10];
	private bool isDone = false;
	private bool wrongAns = false;
	public bool correctDialogue;

	public Texture wrongDialogue;
	private bool wrongDAppear = false;
	public GUIStyle myStyle;
	public GUIStyle myStyle2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!wrongDAppear){
		if (Input.GetMouseButtonDown (0)) {
			
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			
			if (Physics.Raycast (ray, out hit, 100)) {
				
				//			print ("Hit"+hit.transform.tag);
				if (hit.transform.tag == "TopBtn") {
					if (count < 9)
						count++;
					print ("TopBtn");
				}
				if (hit.transform.tag == "BottomBtn") {
					if (count > 0)
						count--;
					print ("BottomBtn");
				}
				if (hit.transform.tag == "Done") {
					isDone = true;
					if (count == 6) {
						correctDialogue = true;
					} else {
						wrongAns = true;
						wrongDAppear = true;
					}
					print ("BottomBtn");
				}
			}
		}
	}
		if (count == 0) {
			numberCube.GetComponent<Renderer>().material = myMaterials[0];
		}
		else
		if (count == 1) {
			numberCube.GetComponent<Renderer>().material = myMaterials[1];
		}
		else
		if (count == 2) {
			numberCube.GetComponent<Renderer>().material = myMaterials[2];
		}
		else
		if (count == 3) {
			numberCube.GetComponent<Renderer>().material = myMaterials[3];
		}
		else
		if (count == 4) {
			numberCube.GetComponent<Renderer>().material = myMaterials[4];
		}
		else
		if (count == 5) {
			numberCube.GetComponent<Renderer>().material = myMaterials[5];
		}
		else
		if (count == 6) {
			numberCube.GetComponent<Renderer>().material = myMaterials[6];
		}
		else
		if (count == 7) {
			numberCube.GetComponent<Renderer>().material = myMaterials[7];
		}
		else
		if (count == 8) {
			numberCube.GetComponent<Renderer>().material = myMaterials[8];
		}
		else
		if (count == 9) {
			numberCube.GetComponent<Renderer>().material = myMaterials[9];
		}
	
	}
	void OnGUI(){
		if (wrongAns) {
			if(wrongDAppear){
			GUI.DrawTexture(new Rect(Screen.width*0.2f,Screen.height*0.2f,Screen.width*0.5f,Screen.height*0.5f),wrongDialogue);
			if(GUI.Button(new Rect(Screen.width*0.43f,Screen.height*0.58f,Screen.width*0.05f,Screen.height*0.05f),"",myStyle)){
				wrongDAppear = false;
			}
				if(GUI.Button(new Rect(Screen.width*0.23f,Screen.height*0.55f,Screen.width*0.45f,Screen.height*0.15f),"",myStyle2)){
					wrongDAppear = false;
				}
			}
		}
	}

}

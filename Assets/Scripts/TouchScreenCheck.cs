﻿using UnityEngine;
using System.Collections;

public class TouchScreenCheck : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


		#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
		if(Input.GetKeyDown("left")){
			Application.LoadLevel(0);
		}
		#else
		for (var i = 0; i < Input.touchCount; ++i) {
			Touch touch = Input.GetTouch(i);
			if (touch.phase == TouchPhase.Began) {

								if (touch.position.x > (Screen.width/2)) {
					Application.LoadLevel(0);
				//do something
					}
			}
		}

		#endif
	
	}
}

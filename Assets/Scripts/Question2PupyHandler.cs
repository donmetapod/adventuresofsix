﻿using UnityEngine;
using System.Collections;

public class Question2PupyHandler : MonoBehaviour {
	
	public Texture wrongDialogue;
	public bool dialogueAppear = false;
	public GUIStyle myStyle;
	public GUIStyle myStyle2;

	public bool wrongAns = false;

	public bool isSurprize = false;
	public bool isCourtisy = false;
	public bool isLove = false;
	public bool isAnger = false;
	public bool isDone = false;

	public GameObject surprizeNormalObj;
	public GameObject surprizeHoverObj;

	public GameObject courtisyNormalObj;
	public GameObject courtisyHoverObj;

	public GameObject loveNormalObj;
	public GameObject loveHoverObj;

	public GameObject angerNormalObj;
	public GameObject angerHoverObj;
	// Use this for initialization
	void Start () {
		print("Dialogue"+dialogueAppear);
	}
	
	// Update is called once per frame
	void Update () {
		if(!dialogueAppear){
			print("Dialogue"+dialogueAppear);
		if (Input.GetMouseButtonDown (0)) {

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, 100)) {

				//			print ("Hit"+hit.transform.tag);
				if (hit.transform.tag == "surprizeNormal") {
					isSurprize = true;
					isCourtisy = false;
					isLove = false;
					isAnger = false;
					print ("SurprizeSlect");
				}
//				if(hit.transform.tag == "surprizeHover"){
//					isSurprize = false;
//					print("SurprizeHoverSlect");
//				}
				if (hit.transform.tag == "courtisyNormal") {
					isSurprize = false;
					isCourtisy = true;
					isLove = false;
					isAnger = false;
					print ("CortisySelect");
				}
//				if(hit.transform.tag == "courtisyHover"){
//					isCourtisy = false;
//					print("CortisyHoverSelect");
//				}
				if (hit.transform.tag == "loveNormal") {
					isSurprize = false;
					isCourtisy = false;
					isLove = true;
					isAnger = false;
					print ("LoveSlect");
				}
//				if(hit.transform.tag == "loveHover"){
//					isLove = false;
//					print("LoveHoverSlect");
//				}
				if (hit.transform.tag == "angerNormal") {
					isSurprize = false;
					isCourtisy = false;
					isLove = false;
					isAnger = true;
					print ("AngerSelect");
				}
//				if(hit.transform.tag == "angerHover"){
//					isAnger = false;
//					print("AngerHoverSelect");
//				}
				if (hit.transform.tag == "Done") {
					isDone = true;
					if (isCourtisy) {
						Application.LoadLevel (0);
					} else {
						wrongAns = true;
						dialogueAppear = true;
					}
					print ("DoneSelect");
				}

			}
		}
	}

		if(isSurprize){
			surprizeHoverObj.SetActive(true);
			courtisyNormalObj.SetActive(true);
			loveNormalObj.SetActive(true);
			angerNormalObj.SetActive(true);

			surprizeNormalObj.SetActive(false);
			courtisyHoverObj.SetActive(false);
			loveHoverObj.SetActive(false);
			angerHoverObj.SetActive(false);

		}
		if (isCourtisy) {
			surprizeNormalObj.SetActive(true);
			courtisyHoverObj.SetActive(true);
			loveNormalObj.SetActive(true);
			angerNormalObj.SetActive(true);
			
			surprizeHoverObj.SetActive(false);
			courtisyNormalObj.SetActive(false);
			loveHoverObj.SetActive(false);
			angerHoverObj.SetActive(false);
		}

		if (isLove) {
			surprizeNormalObj.SetActive(true);
			courtisyNormalObj.SetActive(true);
			loveHoverObj.SetActive(true);
			angerNormalObj.SetActive(true);
			
			surprizeHoverObj.SetActive(false);
			courtisyHoverObj.SetActive(false);
			loveNormalObj.SetActive(false);
			angerHoverObj.SetActive(false);
		}

		if (isAnger) {
			surprizeNormalObj.SetActive(true);
			courtisyNormalObj.SetActive(true);
			loveNormalObj.SetActive(true);
			angerHoverObj.SetActive(true);
			
			surprizeHoverObj.SetActive(false);
			courtisyHoverObj.SetActive(false);
			loveHoverObj.SetActive(false);
			angerNormalObj.SetActive(false);
		}

//		if (isDone) {
//			if(isCourtisy){
//				Application.LoadLevel(0);
//			}
//			else{
//				wrongAns = true;
//				dialogueAppear = true;
//			}
//		}
	
	}

	void OnGUI(){
		if(wrongAns){
			if(dialogueAppear){
			GUI.DrawTexture(new Rect(Screen.width*0.2f,Screen.height*0.2f,Screen.width*0.5f,Screen.height*0.5f),wrongDialogue);
			if(GUI.Button(new Rect(Screen.width*0.43f,Screen.height*0.58f,Screen.width*0.05f,Screen.height*0.05f),"",myStyle)){
					dialogueAppear = false;
			}
				if(GUI.Button(new Rect(Screen.width*0.2f,Screen.height*0.52f,Screen.width*0.5f,Screen.height*0.15f),"",myStyle2)){
					dialogueAppear = false;
				}
			}

		}
	}
}

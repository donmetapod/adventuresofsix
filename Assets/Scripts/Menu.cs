﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public Texture BG;
	public Texture BG2;
	public Rect BGPos;
	public Rect BGPos2;

	public Rect textPos;
	public GUIStyle textStyle;
	private float screenWidth = 2048f;
	private float screenHeight = 1536f;
	public GUIStyle goldFishStyle;
	public Rect goldFish;
	public GUIStyle sixPupyStyle;
	public Rect sixPupy;
	public GUIStyle sixShopingStyle;
	public Rect sixShoping;
	public GUIStyle sixParisStyle;
	public Rect sixParis;
	public GUIStyle sixSpainStyle;
	public Rect sixSpain;
	public GUIStyle sixTidesStyle;
	public Rect sixTides;
	public GUIStyle sixLasVegasStyle;
	public Rect sixLasVegas;
	public GUIStyle BookmarkStyle;
	public Rect BookMark;

    private AudioSource source { get { return GetComponent<AudioSource>(); } }
    public AudioClip sound;

    // Use this for initialization
    void Start ()
    {
        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		//float rx = Screen.width / screenWidth;
		//float ry = Screen.height / screenHeight;
		
		//GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

		GUI.DrawTexture (new Rect(0,0,Screen.width*1f,Screen.height*1f), BG);
		//GUI.DrawTexture (BGPos2, BG2);
		//goldFishStyle.fontSize=  Mathf.Min(Mathf.FloorToInt(Screen.width * 50/1280), Mathf.FloorToInt(Screen.height * 60/800));
		//GUI.Label (textPos, "");

		if(GUI.Button(new Rect(Screen.width*0.097f,Screen.height*0.242f,Screen.width*0.3961f,Screen.height*0.11f), "",goldFishStyle))
		{
            Playsound();
            StartCoroutine(changeLevel(0.5f, 1));
		}
		if(GUI.Button(new Rect(Screen.width*0.5067f,Screen.height*0.242f,Screen.width*0.3961f,Screen.height*0.11f), "",sixPupyStyle))
		{
            Playsound();
            StartCoroutine(changeLevel(0.5f, 9));
        }
		if(GUI.Button(new Rect(Screen.width*0.097f,Screen.height*0.4068f,Screen.width*0.3961f,Screen.height*0.11f), "",sixShopingStyle))
		{
			Playsound();
			StartCoroutine(changeLevel(0.5f, 35));
		}
		if(GUI.Button(new Rect(Screen.width*0.5067f,Screen.height*0.4068f,Screen.width*0.3961f,Screen.height*0.11f), "",sixParisStyle))
		{
            Playsound();
            StartCoroutine(changeLevel(0.5f, 19));
        }
		if(GUI.Button(new Rect(Screen.width*0.097f,Screen.height*0.573f,Screen.width*0.3961f,Screen.height*0.11f), "",sixSpainStyle))
		{

		}
		if(GUI.Button(new Rect(Screen.width*0.5067f,Screen.height*0.573f,Screen.width*0.3961f,Screen.height*0.11f), "",sixTidesStyle))
		{
            Playsound();
            StartCoroutine(changeLevel(0.5f, 11));
        }

		if(GUI.Button(new Rect(Screen.width*0.097f,Screen.height*0.741f,Screen.width*0.3961f,Screen.height*0.11f), "",sixLasVegasStyle))
		{

		}
		if(GUI.Button(new Rect(Screen.width*0.5067f,Screen.height*0.741f,Screen.width*0.3961f,Screen.height*0.11f), "",BookmarkStyle))
		{

		}



	/*	//Six And Gold Fish
		if (GUI.Button (new Rect (Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.1f), "Six And Gold Fish")) {
		
		
		}
		//Six And Gold Fish
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.1f), "Six And Puppy")) {
			
			
		}
		//Six Goes Shoping
		if (GUI.Button (new Rect (Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.25f), "Six Goes Shoping")) {
			
			
		}
		//Six Goes To paris
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.25f), "Six Goes To Paris")) {
			
			
		}
		//Six Goes To Spain
		if (GUI.Button (new Rect (Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.4f), "Six Goes To Spain")) {
			
			
		}
		//Six Tides Up
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.4f), "Six Tides Up")) {
			
			
		}
		//Six Goes to Las Vegas
		if (GUI.Button (new Rect (Screen.width * 0.1f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.65f), "Six Goes To Las Vegas")) {
			
			
		}
		//Go to BookMark
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.1f, Screen.width * 0.25f, Screen.height * 0.65f), "Go To Book Mark")) {
			
			
		}*/

	}

    void Playsound()
    {
        source.PlayOneShot(sound);
    }

    IEnumerator changeLevel(float wait, int level)
    {
        yield return new WaitForSeconds(wait);
        Application.LoadLevel(level);
    }
}


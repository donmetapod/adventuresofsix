﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleCustomNarration : MonoBehaviour {

	public void ToggleNarration(){
		if (PlayerPrefs.GetInt ("AltNarration") == 1) {
			PlayerPrefs.SetInt ("AltNarration", 0);
		} else {
			PlayerPrefs.SetInt ("AltNarration", 1);
		}
		PlayerPrefs.Save ();
	}
}

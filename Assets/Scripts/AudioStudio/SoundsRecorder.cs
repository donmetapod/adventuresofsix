using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class SoundsRecorder : MonoBehaviour {

	public MyScriptableObjectClass[] chapters;
	public Text recordingPingPong;
	public Image recordingGlowPingPong;
	public Image onTheAirImage;
	public Text noAudioRecorded;
	public Text audioSaved;
	
	[SerializeField]Text title;
	[SerializeField]Text main;
	[SerializeField]Text page;
	[SerializeField]int currentPage = 0;
	[SerializeField]int currentBook = 0;
	[SerializeField] private Button recordingButton;
	[SerializeField] private Button playButton;
	[SerializeField] private Button stopButton;
	[SerializeField] private Button saveButton;
	[SerializeField] private Button nextPageButton;
	[SerializeField] private Button prevPageButton;
	
	AudioClip myAudioClip;
	int pageNumber;
	string formatedString;
	bool currentlyRecording;

	void Start(){
		if (PlayerPrefs.GetInt("AltNarration") == 0)
		{
			PlayerPrefs.SetInt("AltNarration", 1);
			PlayerPrefs.Save();
		}
		title.text = chapters [0].book;
		formatedString = chapters [0].voText[currentPage];
		formatedString = formatedString.Replace("NEWLINE","\n\n");
		main.text = formatedString;
		pageNumber = currentPage + 1;
		page.text = "Page " + pageNumber + " of " + chapters[currentBook].voText.Length;
	}

	public void StartRecording()
	{
		recordingButton.interactable = false;
		playButton.interactable = false;
		//saveButton.interactable = false;
		nextPageButton.interactable = false;
		prevPageButton.interactable = false;
		
		if (currentlyRecording)
		{
			SaveAudio();
		}
		else
		{
			currentlyRecording = true;
			recordingPingPong.text = "Recording audio";
			recordingPingPong.gameObject.SetActive (true);
			recordingGlowPingPong.gameObject.SetActive (true);
			onTheAirImage.gameObject.SetActive (true);
			myAudioClip = Microphone.Start ( null, false, 10, 44100 );	
		}
		
	}

	public void PlaySound(){
		if (currentlyRecording)
		{
			SaveAudio();
		}
		string path = "file://" + Application.persistentDataPath + "/" + chapters [currentBook].audioName + "_" + currentPage + ".wav";
		print("playing audio: " + path);

		StartCoroutine(LoadAudioAndPlay(path));
	}
	
	IEnumerator LoadAudioAndPlay(string url){
		WWW www = new WWW(url);
		AudioClip loadedAudioClip = WWWAudioExtensions.GetAudioClip(www);
		if (loadedAudioClip == null)
		{
			print("no audio");
			recordingPingPong.gameObject.SetActive (true);
			recordingPingPong.text = "No audio recorded for this chapter";
			Invoke("HideNoAudioRecordedMessage", 3);
		}
		else
		{
			while (loadedAudioClip.loadState != AudioDataLoadState.Loaded) {
				yield return new WaitForEndOfFrame();
			}
			//yield return new WaitForSeconds(3);
			GetComponent<AudioSource> ().clip = loadedAudioClip;
			GetComponent<AudioSource> ().Play ();	
		}
		
	}
	
	void PlayAudio(){
		GetComponent<AudioSource> ().Play ();
	}

	void HideNoAudioRecordedMessage(){
		noAudioRecorded.gameObject.SetActive (false);
	}

	public void StopRecording(){
		if (Microphone.IsRecording("Built-in Microphone")) {
			Microphone.End ("Built-in Microphone");
			recordingPingPong.gameObject.SetActive (true);
			recordingPingPong.text = "Saving your audio";
			//Invoke("HideNoAudioRecordedMessage", 4);
			SaveAudio ();
		}
	}

	public void SaveAudio()
	{
		currentlyRecording = false;
		recordingPingPong.gameObject.SetActive (false);
		recordingGlowPingPong.gameObject.SetActive (false);
		onTheAirImage.gameObject.SetActive (false);
		Microphone.End ("Built-in Microphone");
		SavWav.Save(chapters [currentBook].audioName + "_" + currentPage, myAudioClip);
		print("saving audio: " + chapters[currentBook].audioName + "_" + currentPage);
		audioSaved.gameObject.SetActive (true);
		Invoke ("HideSavedAudioMessage", 4);
		ButtonPressed("Next");
	}

	void HideSavedAudioMessage(){
		audioSaved.gameObject.SetActive (false);
		recordingButton.interactable = true;
		playButton.interactable = true;
		saveButton.interactable = true;
		nextPageButton.interactable = true;
		prevPageButton.interactable = true;
	}

	public void ButtonPressed(string buttonName){
		switch (buttonName) {
		case "Next":
			if (currentlyRecording)
			{
				SaveAudio();
			}
			if (currentPage < chapters [currentBook].voText.Length - 1) {
				currentPage++;
			} else {
				if (currentBook < chapters.Length - 1) {
					currentPage = 0;
					currentBook++;
				}
			}
			title.text = chapters [currentBook].book;
			formatedString = chapters [currentBook].voText [currentPage];
			formatedString = formatedString.Replace("NEWLINE","\n\n");
			main.text = formatedString;
			pageNumber = currentPage + 1;
			page.text = "Page " + pageNumber + " of " + chapters[currentBook].voText.Length;
			
			break;
		case "Prev":
			if (currentPage > 0) {
				currentPage--;
				title.text = chapters [currentBook].book;
				formatedString = chapters [currentBook].voText [currentPage];
				formatedString = formatedString.Replace("NEWLINE","\n\n");
				main.text = formatedString;
				pageNumber = currentPage + 1;
				page.text = "Page " + pageNumber + " of " + chapters[currentBook].voText.Length;
			} else {
				if (currentBook > 0) {
					currentBook--;
					currentPage = chapters [currentBook].voText.Length-1;
					title.text = chapters [currentBook].book;
					formatedString = chapters [currentBook].voText [currentPage];
					formatedString = formatedString.Replace("NEWLINE","\n\n");
					main.text = formatedString;
					pageNumber = currentPage + 1;
					page.text = "Page " + pageNumber + " of " + chapters[currentBook].voText.Length;
				}
			}
			break;
			case "Exit":
				SceneManager.LoadScene("MainSettings03");
				break;
				
		}
	}
}

[Serializable]
class AudioClipSample  
{
	public int frequency;
	public int samples;
	public int channels;
	public float[] sample;
}
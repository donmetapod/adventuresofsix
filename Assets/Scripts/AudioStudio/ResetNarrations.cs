﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetNarrations : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		PlayerPrefs.SetInt("AltNarration", 0);
		PlayerPrefs.Save();
	}

}

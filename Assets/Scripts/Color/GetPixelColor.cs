﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPixelColor : MonoBehaviour
{
	private Vector3 mpos = new Vector3();
	[SerializeField]Image[] skinImages;


	public void DoGetColor()
	{
		StartCoroutine(GetColor());
	}

	IEnumerator GetColor()
	{
		mpos = Input.mousePosition;
		Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		yield return new WaitForEndOfFrame();
		tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		tex.Apply();
 
		var bla = tex.GetPixel  ( (int)mpos.x, (int)mpos.y );
		
		PlayerPrefs.SetFloat("r", bla.r);
		PlayerPrefs.SetFloat("g", bla.g);
		PlayerPrefs.SetFloat("b", bla.b);
		PlayerPrefs.Save();
		skinImages [0].color = bla;
		skinImages [1].color = bla;
		print("color applied");
	}
}

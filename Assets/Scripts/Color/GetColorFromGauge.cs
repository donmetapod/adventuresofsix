﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetColorFromGauge : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		GetComponent<Image>().color =
			new Color(PlayerPrefs.GetFloat("r"), PlayerPrefs.GetFloat("g"), PlayerPrefs.GetFloat("b"));
	}
}

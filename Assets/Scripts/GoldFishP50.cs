﻿using UnityEngine;
using System.Collections;

public class GoldFishP50 : MonoBehaviour {

	
	/// <summary>
	/// DrawTextures All for Menu	/// </summary>
	
	public Texture homeText;
	public Texture restartText;
	public Texture settingText;
	public Texture bookMarkText;
	
	public Rect homeTextRect;
	public Rect restartTextRect;
	public Rect settingTextRect;
	public Rect bookMarkTextRect;
	
	
	/// <summary>
	/// T/////end	/// </summary>
	private bool ansTrue = false;
	private float screenWidth = 2048f;
	private float screenHeight = 1536f;

	public GameObject plane1;
	public GameObject plane2;
	private bool isQuestion = false;
	private bool next1 = false;
	private int count = 0;
	
	public Texture BG1;
	public Rect BG1Pos;
	
	public Texture BG2;
	public Rect BG2Pos;
	
	public Texture text1;
	public Rect text1Pos;
	
	public Texture text2;
	public Rect text2Pos;
	
	public GUIStyle btn1Style;
	public Rect btn1Pos;
	
	public Texture dialogue1;
	public Rect dialoguePos;
	
	public Texture dialogue2;
	public Rect dialogue2Pos;
	
	public GUIStyle next1Style;
	public Rect next1Pos;
	
	
	//Menu Variables
	private bool isMenu = false;
	
	public Texture menuBG;
	public Rect menuBGPos;
	
	public GUIStyle menuIconStyle;
	public Rect menuIconPos;
	public GUIStyle menuIconStyle2;
	public Rect menuIconPos2;
	
	public GUIStyle bookMarkStyle;
	public Rect bookMarkPos;
	
	public GUIStyle restartStyle;
	public Rect restartPos;
	
	public GUIStyle settingStyle;
	public Rect settingPos;
	
	public GUIStyle homeStyle;
	public Rect homePos;
	
	public GUIStyle crossStyle;
	public Rect crossPos;
	//end off Menu variables

	public GUISkin mySkin;
	private bool isLeft = false;
	private bool wrongAns = false;
	
	public Texture wrongDialogue;
	public Rect wrongDiaPos;
	
	public GUIStyle btnOkStyle;
	public GUIStyle btnOkStyle2;
	public Rect btnOkPos2;
	public Rect btnOkPos;

	public Rect leftPos;
	public Rect leftTickPos;
	
	public Rect rightPos;
	public Rect rightTickPos;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100)) {
				print ("Hit"+hit.transform.tag);
				if(hit.transform.tag == "CubeQuestion1"){
					isQuestion = true;
				}
			}
		}
		
	}
	
	void OnGUI(){
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));
		
		if (!isQuestion) {
			plane1.SetActive(true);
			plane2.SetActive(false);
			
//			if (GUI.Button (btn1Pos, "", btn1Style)) {
//				isQuestion = true;
//				
//			}
		}
		
		if(isQuestion){
			plane1.SetActive(false);
			plane2.SetActive(true);
			GUI.DrawTexture (BG2Pos, BG2);
			if(!wrongAns){
			GUI.DrawTexture (dialoguePos, dialogue1);
				if(!ansTrue){
			if (isLeft) {
				if (GUI.Button (leftPos, "", mySkin.customStyles [16])) {
					
				}
				
				if (GUI.Button (rightTickPos, "", mySkin.customStyles [17])) {
					isLeft = false;
					print ("leftFalse");
					//isLeft = false;
				}
			} else {
				if (GUI.Button (leftPos, "", mySkin.customStyles [19])) {
					isLeft = true;
					print ("leftTrue");
					//	isLeft = false;
				}
				
				if (GUI.Button (rightTickPos, "", mySkin.customStyles [18])) {
					
				}
				
			}
			}
			
			if(GUI.Button(next1Pos,"",next1Style)){
					if (isLeft) {
						next1 = true;
						count++;
						print ("MyCount" + count);
					} else {
						wrongAns = true;
					}
				if(count == 2){
					Application.LoadLevel(8);
				}
			}
			}
			
		}
		
		if (next1) {
			GUI.DrawTexture(dialogue2Pos,dialogue2);
			ansTrue = true;
			
		}

		if (wrongAns) {
			GUI.DrawTexture (wrongDiaPos, wrongDialogue);
			if(GUI.Button(btnOkPos2,"",btnOkStyle)){
				//wrongAns = false;
			}
			if(GUI.Button(btnOkPos,"O K",btnOkStyle2)){
				wrongAns = false;
			}
		}
		
		//Menu Pannel
		if (!isMenu) {
			if (GUI.Button (menuIconPos, "", menuIconStyle)) {
				isMenu = true;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = true;
				
			}
		}
		
		if(isMenu){
			
			GUI.DrawTexture(menuBGPos,menuBG);

			GUI.DrawTexture(homeTextRect,homeText);
			GUI.DrawTexture(restartTextRect,restartText);
			GUI.DrawTexture(settingTextRect,settingText);
			GUI.DrawTexture(bookMarkTextRect,bookMarkText);
			if (GUI.Button (crossPos, "", crossStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = false;
				
			}
			if (GUI.Button (bookMarkPos, "", bookMarkStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (restartPos, "", restartStyle)) {
				isMenu = false;
				Application.LoadLevel(2);
				
			}
			if (GUI.Button (settingPos, "", settingStyle)) {
				isMenu = false;
				Application.LoadLevel(1);
				
			}
			if (GUI.Button (homePos, "", homeStyle)) {
				isMenu = false;
				Application.LoadLevel(0);
				
			}
			
		}
		// end of menu Panel
		
		
	}
}

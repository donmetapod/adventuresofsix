﻿using UnityEngine;
using System.Collections;

public class PupyPage1 : MonoBehaviour {
	private float screenWidth = 1280f;
	private float screenHeight = 800f;

	public Texture BG1;
	public Rect BG1Pos;

	public Texture text1;
	public Texture text2;

	public Rect text1Pos;
	public Rect text2Pos;

	public Rect lable1;
	public GUIStyle myStyle;
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

		GUI.DrawTexture (BG1Pos,BG1);
		GUI.DrawTexture (text1Pos, text1);
		GUI.DrawTexture (text2Pos, text2);

		//GUI.Label (lable1,"Pencil",myStyle);
	}
}

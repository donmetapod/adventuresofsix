﻿using UnityEngine;
using System.Collections;

public class PuppyP1 : MonoBehaviour {

	public GameObject q2Plane3;
	public GameObject q2Plane;
	public GameObject plane2;
	public GameObject question1Plane;
	private float screenWidth = 1280f;
	private float screenHeight = 800f;
	private bool isQuestion = false;
	private bool next1 = false;
	private bool next2 = false;
	private int count = 0;

	public Texture BG1;
	public Rect BG1Pos;

	public Texture BG2;
	public Rect BG2Pos;

	public Texture text1;
	public Rect text1Pos;

	public GUIStyle btn1Style;
	public Rect btn1Pos;

	public Texture dialogue1;
	public Rect dialoguePos;

	public Texture dialogue2;
	public Rect dialogue2Pos;

	public Texture dialogue3;
	public Rect dialogue3Pos;



	public GUIStyle next1Style;
	public Rect next1Pos;


	//Menu Variables
	private bool isMenu = false;

	public Texture menuBG;
	public Rect menuBGPos;

	public GUIStyle menuIconStyle;
	public Rect menuIconPos;
	public GUIStyle menuIconStyle2;
	public Rect menuIconPos2;
	
	public GUIStyle bookMarkStyle;
	public Rect bookMarkPos;
	
	public GUIStyle restartStyle;
	public Rect restartPos;
	
	public GUIStyle settingStyle;
	public Rect settingPos;
	
	public GUIStyle homeStyle;
	public Rect homePos;
	
	public GUIStyle crossStyle;
	public Rect crossPos;
	//end off Menu variables

	//Dialogue

	public GUISkin DialogueSkin;
	public Rect pencilPos;
	public Rect pencilTickPos;
	public Rect gamesPos;
	public Rect gamesTickPos;
	public Rect paperPos;
	public Rect paperTickPos;
	public Rect pensPos;
	public Rect pensTickPos;
	public Rect lunchPos;
	public Rect lunchTickPos;
	public Rect bookPos;
	public Rect bookTickPos;
	public Rect homeWorkPos;
	public Rect homeWorkTickPos;
	public Rect snacksPos;
	public Rect snacksTickPos;

	private bool pencil = true;
	private bool games = false;
	private bool paper = false;
	private bool pens = false;
	private bool lunch = true;
	private bool book = false;
	private bool homeWork = false;
	private bool snack = false;

	private bool wrongAns = false;

	public Texture wrongDialogue;
	public Rect wrongDiaPos;

	public GUIStyle btnOkStyle;
	public GUIStyle btnOkStyle2;
	public Rect btnOkPos2;
	public Rect btnOkPos;

	public Rect next2Btn;
	private bool isQuestion2 = false;
	void OnTriggerEnter(Collider other) {

	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 100)) {
//			print ("Hit"+hit.transform.tag);
			if(hit.transform.tag == "CubeQuestion1"){
			isQuestion = true;
			}
				if(hit.transform.tag == "CubeQuestion2"){
					isQuestion2 = true;
					q2Plane3.SetActive(true);
				}
		}
		}
	
	}

	void OnGUI(){
		float rx = Screen.width / screenWidth;
		float ry = Screen.height / screenHeight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

		if (!isQuestion) {
			question1Plane.SetActive(true);
			plane2.SetActive(false);

//			if (GUI.Button (btn1Pos, "", btn1Style)) {
//				isQuestion = true;
//		
//			}
		}

		if (!isQuestion2) {
			q2Plane.SetActive(false);
			q2Plane3.SetActive(false);
			question1Plane.SetActive(true);
		}
		if(isQuestion2){
			q2Plane.SetActive(true);
			question1Plane.SetActive(false);
			q2Plane3.SetActive(true);

		}

		if (isQuestion) {
			question1Plane.SetActive(false);
			plane2.SetActive(true);
			//GUI.DrawTexture (BG2Pos, BG2);
			if(!wrongAns){
			GUI.DrawTexture (dialoguePos, dialogue1);
			if (pencil) {
//				if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
//				
//					print ("pencil true");
//				}
					if (GUI.Button (pencilTickPos, "", DialogueSkin.customStyles [1])) {

					}
					if (GUI.Button (gamesPos, "", DialogueSkin.customStyles [2])) {
						games = true;
						pencil = false;
						paper = false;
						pens = false;
						print ("games true");
					}
					if (GUI.Button (paperPos, "", DialogueSkin.customStyles [4])) {
						paper = true;
						pencil = false;
						games = false;
						pens = false;
											print ("paper true");
					}
					if (GUI.Button (pensPos, "", DialogueSkin.customStyles [6])) {
						pens = true;
						paper = false;
						pencil = false;
						games = false;
											print ("pens true");
										}
//					if (GUI.Button (gamesTickPos, "", DialogueSkin.customStyles [3])) {
//						games = false;
//					}
			} /////////// pencil end

				if (games) {
					//				if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
					//				
					//					print ("pencil true");
					//				}
					if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
						games = false;
						pencil = true;
						paper = false;
						pens = false;
					}
					if (GUI.Button (gamesTickPos, "", DialogueSkin.customStyles [3])) {

						print ("games true");
					}
					if (GUI.Button (paperPos, "", DialogueSkin.customStyles [4])) {
						paper = true;
						pencil = false;
						games = false;
						pens = false;
						print ("paper true");
					}
					if (GUI.Button (pensPos, "", DialogueSkin.customStyles [6])) {
						pens = true;
						paper = false;
						pencil = false;
						games = false;
						print ("pens true");
					}
					//					if (GUI.Button (gamesTickPos, "", DialogueSkin.customStyles [3])) {
					//						games = false;
					//					}
				} /////////// games end

				if (paper) {
					//				if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
					//				
					//					print ("pencil true");
					//				}
					if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
						games = false;
						pencil = true;
						paper = false;
						pens = false;
					}
					if (GUI.Button (gamesPos, "", DialogueSkin.customStyles [2])) {
						paper = false;
						pencil = false;
						games = true;
						pens = false;
						print ("games true");
					}
					if (GUI.Button (paperTickPos, "", DialogueSkin.customStyles [5])) {

						print ("paper true");
					}
					if (GUI.Button (pensPos, "", DialogueSkin.customStyles [6])) {
						pens = true;
						paper = false;
						pencil = false;
						games = false;
						print ("pens true");
					}
					//					if (GUI.Button (gamesTickPos, "", DialogueSkin.customStyles [3])) {
					//						games = false;
					//					}
				} /////////// paper end

				if (pens) {
					//				if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
					//				
					//					print ("pencil true");
					//				}
					if (GUI.Button (pencilPos, "", DialogueSkin.customStyles [0])) {
						games = false;
						pencil = true;
						paper = false;
						pens = false;
					}
					if (GUI.Button (gamesPos, "", DialogueSkin.customStyles [2])) {
						paper = false;
						pencil = false;
						games = true;
						pens = false;
						print ("games true");
					}
					if (GUI.Button (paperPos, "", DialogueSkin.customStyles [4])) {
						pens = false;
						paper = true;
						pencil = false;
						games = false;
						print ("paper true");
					}
					if (GUI.Button (pensTickPos, "", DialogueSkin.customStyles [7])) {

						print ("pens true");
					}
					//					if (GUI.Button (gamesTickPos, "", DialogueSkin.customStyles [3])) {
					//						games = false;
					//					}
				} /////////// paper end



				///////////////////////////used/////////////////
				/// 
				/// ///////////////Questuion2////////////////////

			if (lunch) {

				if (GUI.Button (lunchTickPos, "", DialogueSkin.customStyles [9])) {
					
				}
				if (GUI.Button (bookPos, "", DialogueSkin.customStyles [10])) {
					book = true;
					lunch = false;
					homeWork = false;
					snack = false;
					print ("book true");
				}
				if (GUI.Button (homeWorkPos, "", DialogueSkin.customStyles [12])) {
						book = false;
						lunch = false;
						homeWork = true;
						snack = false;
					print ("homework true");
				}
				if (GUI.Button (snacksPos, "", DialogueSkin.customStyles [14])) {
						book = false;
						lunch = false;
						homeWork = false;
						snack = true;
					print ("snack true");
				}
				}

				if (book) {
					
					if (GUI.Button (lunchPos, "", DialogueSkin.customStyles [8])) {
						book = false;
						lunch = true;
						homeWork = false;
						snack = false;
					}
					if (GUI.Button (bookTickPos, "", DialogueSkin.customStyles [11])) {

						print ("book true");
					}
					if (GUI.Button (homeWorkPos, "", DialogueSkin.customStyles [12])) {
						book = false;
						lunch = false;
						homeWork = true;
						snack = false;
						print ("homework true");
					}
					if (GUI.Button (snacksPos, "", DialogueSkin.customStyles [14])) {
						book = false;
						lunch = false;
						homeWork = false;
						snack = true;
						print ("snack true");
					}
				}///////////// end of book////////////

				if (homeWork) {
					
					if (GUI.Button (lunchPos, "", DialogueSkin.customStyles [8])) {
						book = false;
						lunch = true;
						homeWork = false;
						snack = false;
					}
					if (GUI.Button (bookPos, "", DialogueSkin.customStyles [10])) {
						book = true;
						lunch = false;
						homeWork = false;
						snack = false;
						print ("book true");
					}
					if (GUI.Button (homeWorkTickPos, "", DialogueSkin.customStyles [13])) {

						print ("homework true");
					}
					if (GUI.Button (snacksPos, "", DialogueSkin.customStyles [14])) {
						book = false;
						lunch = false;
						homeWork = false;
						snack = true;
						print ("snack true");
					}
				}///////////home work end///////////////////

				if (snack) {
					
					if (GUI.Button (lunchPos, "", DialogueSkin.customStyles [8])) {
						book = false;
						lunch = true;
						homeWork = false;
						snack = false;
					}
					if (GUI.Button (bookPos, "", DialogueSkin.customStyles [10])) {
						book = true;
						lunch = false;
						homeWork = false;
						snack = false;
						print ("book true");
					}
					if (GUI.Button (homeWorkPos, "", DialogueSkin.customStyles [12])) {
						book = false;
						lunch = false;
						homeWork = true;
						snack = false;
						print ("homework true");
					}
					if (GUI.Button (snacksTickPos, "", DialogueSkin.customStyles [15])) {

						print ("snack true");
					}
				}///////////snacks end///////////////////



			

				if(!next1 && !next2){
			if (GUI.Button (next1Pos, "", next1Style)) {
					print("true"+paper + homeWork);
				if (paper && homeWork) {

					next1 = true;
						count++;
				} else {
					wrongAns = true;
//						print("WrongAns"+wrongAns);

				}
				//count++;
//				print ("MyCount" + count);
				if (count == 2) {
					//Application.LoadLevel (4);
				}
			}
			}
		

			

		}
			if(wrongAns){
				GUI.DrawTexture(wrongDiaPos,wrongDialogue);
				if(GUI.Button(btnOkPos2,"",btnOkStyle)){
					wrongAns = false;
				}
				if(GUI.Button(btnOkPos,"",btnOkStyle2)){
					wrongAns = false;
				}
				
			}

		if (next1 && !next2) {
			GUI.DrawTexture(dialogue2Pos,dialogue2);
				if(GUI.Button(next2Btn,"",DialogueSkin.customStyles[16])){
					next2 = true;
				}
		
		}
			if(next2){
				print ("Next2"+next2);
				GUI.DrawTexture(dialogue3Pos,dialogue3);
				if(GUI.Button(next2Btn,"",DialogueSkin.customStyles[16])){
					Application.LoadLevel(Application.loadedLevel);
				}
			}

		
			
		}/////is question end///////////////
		//Menu Pannel
		if (!isMenu) {
			print("PupyMenu");
			if (GUI.Button (menuIconPos, "", menuIconStyle)) {
				isMenu = true;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = true;
				
			}
		}
		
		if(isMenu){
			
			GUI.DrawTexture(menuBGPos,menuBG);
			if (GUI.Button (crossPos, "", crossStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (menuIconPos2, "", menuIconStyle2)) {
				isMenu = false;
				
			}
			if (GUI.Button (bookMarkPos, "", bookMarkStyle)) {
				isMenu = false;
				
			}
			if (GUI.Button (restartPos, "", restartStyle)) {
				isMenu = false;
				Application.LoadLevel(Application.loadedLevel);
				
			}
			if (GUI.Button (settingPos, "", settingStyle)) {
				isMenu = false;
				Application.LoadLevel(1);
				
			}
			if (GUI.Button (homePos, "", homeStyle)) {
				isMenu = false;
				Application.LoadLevel(0);
				
			}
		}//////// end of menu//////////////
		
		
	}/// end of GUI////////////
}

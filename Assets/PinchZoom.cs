﻿using UnityEngine;
using System.Collections;

public class PinchZoom : MonoBehaviour {
	
	public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
	public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.

	private Vector2 ScreenSize;
	//private Vector3 originalPos;
	private GameObject parentObject;

	public bool zoomOut = false;
	bool isMousePressed = false;
	public bool zoomIn = false;
	public bool resetPos = false;
	public float cameraValue;


	void Update()
	{
		print ("View"+GetComponent<Camera>().fieldOfView);
		if(Input.GetMouseButtonDown(0))
			isMousePressed = true;
		else if(Input.GetMouseButtonUp(0))
			isMousePressed = false;
		// These lines of code will pan/drag the object around untill the edge of the image
//		if(Input.touchCount==1 && Input.GetTouch(0).phase == TouchPhase.Moved && camera.fieldOfView <= 40)
//		{
//			Touch touch = Input.GetTouch(0);	
//			Vector3 diff = touch.deltaPosition*0.1f;	
//			Vector3 pos = transform.position + diff;
//			if(pos.x > ScreenSize.x * (parentObject.transform.localScale.x-1))
//				pos.x = ScreenSize.x * (parentObject.transform.localScale.x-1);
//			if(pos.x < ScreenSize.x * (parentObject.transform.localScale.x-1)*-1)
//				pos.x = ScreenSize.x * (parentObject.transform.localScale.x-1)*-1;
//			if(pos.y > ScreenSize.y * (parentObject.transform.localScale.y-1))
//				pos.y = ScreenSize.y * (parentObject.transform.localScale.y-1);
//			if(pos.y < ScreenSize.y * (parentObject.transform.localScale.y-1)*-1)
//				pos.y = ScreenSize.y * (parentObject.transform.localScale.y-1)*-1;
//			transform.position = pos;
//
//			Vector3 posNew = this.transform.position;
//			posNew.x = Mathf.Clamp(posNew.x, -3.73f, 3.24f);
//			posNew.y = Mathf.Clamp(posNew.y, -0.61f, 2.80f);
//			posNew.z = Mathf.Clamp(posNew.z, -4.1508f, -4.1508f);
//			this.transform.position = posNew;
//
//		}
		// If there are two touches on the device...
		if (Input.touchCount == 2)
		{
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			{
				cameraValue = GetComponent<Camera>().fieldOfView;
				// Otherwise change the field of view based on the change in distance between the touches.

					
					GetComponent<Camera>().fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed * Time.deltaTime *10f;
			//	camera.fieldOfView = Mathf.Clamp(camera.fieldOfView - (1 * 2),36,60);
//					camera.fieldOfView = Mathf.Lerp
//						(camera.fieldOfView, deltaMagnitudeDiff * perspectiveZoomSpeed * Time.deltaTime);



				GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 36f, 60f);


//				if(cameraValue >=60){
//					camera.fieldOfView = 59.9f;
//				}
				
				// Clamp the field of view to make sure it's between 0 and 180.

				//				camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, 0.1f, 179.9f);
			}
			
			if(GetComponent<Camera>().fieldOfView == 60f)
			{
				zoomIn = false;
//				zoomOut = true;
//				this.transform.position = new Vector3(47.35f, 20.96f, 22.69f);
			}
			if(GetComponent<Camera>().fieldOfView >= 60f)
			{
				resetPos = true;
				//				zoomOut = true;
				//				this.transform.position = new Vector3(47.35f, 20.96f, 22.69f);
			}
			else{
				resetPos = false;
			}
			if(GetComponent<Camera>().fieldOfView == 36f)
			{
				zoomIn = true;
			}
		}
	}
}

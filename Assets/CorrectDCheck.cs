﻿using UnityEngine;
using System.Collections;

public class CorrectDCheck : MonoBehaviour {
	public PuppyP2Q2Handler checkAns;
	public GameObject QDialogue;
	public GameObject AnsDialogue;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (checkAns.correctDialogue) {
			QDialogue.SetActive(false);
			AnsDialogue.SetActive(true);
		}

		if (Input.GetMouseButtonDown (0)) {
			
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			
			if (Physics.Raycast (ray, out hit, 100)) {
				
				//			print ("Hit"+hit.transform.tag);
				if (hit.transform.tag == "CorrectOkBtn") {
					Application.LoadLevel(0);
					print ("CorrectOkBtn");
				}
			}
		}
	
	}
}

﻿using UnityEngine;
using System.Collections;
public class PinchToZoo3 : MonoBehaviour 
{
	public int speed = 4;
	public float speedMine = 0f;
	public Camera selectedCamera;
	public float MINSCALE = 2.0F;
	public float MAXSCALE = 5.0F;
	public float minPinchSpeed = 5.0F;
	public float varianceInDistances = 5.0F;
	private float touchDelta = 0.0F;
	private Vector2 prevDist = new Vector2(0,0);
	private Vector2 curDist = new Vector2(0,0);
	private float speedTouch0 = 0.0F;
	private float speedTouch1 = 0.0F;
	Vector3 mousPos;
	Vector3 startPos;
	//public GameObject gameCamera;

	public bool isPinch = false;

	// Use this for initialization
	void Start () 
	{
		startPos = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{//gameCamera.transform.position = Vector3.Lerp(new Vector3(gameCamera.transform.position.x, gameCamera.transform.position.y, gameCamera.transform.position.z), new Vector3(-0.3292148f, 2.398909f,-4.542806f), 0.1f);
		//gameCamera.transform.position = Vector3.Lerp(new Vector3(gameCamera.transform.position.x, gameCamera.transform.position.y, gameCamera.transform.position.z), new Vector3(0.58f, 0.58f,-4.26f), 0.1f);
		//if(Input.GetMouseButtonDown(0))
		//{
		//	mousPos = Input.mousePosition;
		//gameCamera.transform.position = Vector3.Lerp(new Vector3(gameCamera.transform.position.x, gameCamera.transform.position.y, gameCamera.transform.position.z), new Vector3(-0.78f, 2.25f,-4.53f), 0.1f);
//			print(mousPos);
//			print(selectedCamera.fieldOfView);
			if(Input.mousePosition.x>=20){
				//gameCamera.transform.position = Vector3.Lerp(new Vector3(gameCamera.transform.position.x, gameCamera.transform.position.y, gameCamera.transform.position.z), new Vector3(0.023319f, 1.1103f,-4.1508f), 0.1f);
			}
			if(selectedCamera.fieldOfView <= 40){
			//gameCamera.transform.position = Vector3.Lerp(new Vector3(gameCamera.transform.position.x, gameCamera.transform.position.y, gameCamera.transform.position.z), new Vector3(0.58f, 0.58f,-4.26f), 0.1f);
			}
			else{
		//	gameCamera.transform.position = Vector3.Lerp(new Vector3(gameCamera.transform.position.x, gameCamera.transform.position.y, gameCamera.transform.position.z), new Vector3(-0.78f, 2.25f,-4.53f), 0.1f);
			}

		if (Input.touchCount == 2) 
		{
			isPinch = false;
		}
		if(Input.touchCount < 2  && selectedCamera.fieldOfView < 50)
		{
			isPinch = true;
		}
		if(!isPinch)
		{
			Zoom();
		}
		if(isPinch)
		{
			//MouseMove();
		}

		if(selectedCamera.fieldOfView >= 55 && selectedCamera.fieldOfView <= 60 && !isPinch)
		{
			Vector3.Lerp( this.transform.position, startPos, 0.1f);
		}
		//}
	
	}

	void Zoom()
	{
		if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved)
		{
			curDist = Input.GetTouch(0).position - Input.GetTouch(1).position; //current distance between finger touches
			prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition)); //difference in previous locations using delta positions
			touchDelta = curDist.magnitude - prevDist.magnitude;
			speedTouch0 = Input.GetTouch(0).deltaPosition.magnitude / Input.GetTouch(0).deltaTime;
			speedTouch1 = Input.GetTouch(1).deltaPosition.magnitude / Input.GetTouch(1).deltaTime;
			
			
			if ((touchDelta + varianceInDistances <= 1) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
			{
				
				selectedCamera.fieldOfView = Mathf.Clamp(selectedCamera.fieldOfView + (1 * speed),20,60);
				isPinch = true;
			}
			
			if ((touchDelta +varianceInDistances > 1) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
			{
				isPinch = true;
				selectedCamera.fieldOfView = Mathf.Clamp(selectedCamera.fieldOfView - (1 * speed),20,60);
			}
			
		} 
	}

	void MouseMove()
	{

		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved) 
		{
			Vector3 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			transform.Translate(-touchDeltaPosition.x * speedMine, -touchDeltaPosition.y * speedMine, 0);

			Vector3 pos = this.transform.position;
			pos.x = Mathf.Clamp(pos.x, -4.24f, 4.11f);
			pos.y = Mathf.Clamp(pos.y, -0.78f, 3.16f);
			pos.z = Mathf.Clamp(pos.z, -4.1508f, -4.1508f);
			this.transform.position = pos;
		}
	}
}
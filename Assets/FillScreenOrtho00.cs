﻿using UnityEngine;
using System.Collections;

public class FillScreenOrtho00 : MonoBehaviour {
	float height;
	float width;
	bool isIpad = false;
	// Use this for initialization
	void Start () {
		height = (float)Camera.main.orthographicSize* 2.0f;
		 width = height* Screen.width / Screen.height;
		transform.localScale = new Vector3(width/9.9f, 1.0f, height/9.9f);
        //Previous IOS definition
        //if (UnityEngine.iOS.Device.generation.ToString().IndexOf("iPad") > -1)
        //Generalized implementetion for cross platform
        if (SystemInfo.deviceModel.IndexOf("iPad") > -1)
        {
            isIpad = true;
        }
	}

	void OnGUI(){

	}
	
	// Update is called once per frame
	void Update () {

		//transform.localScale = new Vector3(Camera.main.orthographicSize * 2.0f * Screen.width / Screen.height, Camera.main.orthographicSize * 2.0f, 1f);


	}
}

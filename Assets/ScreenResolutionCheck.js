﻿#pragma strict

function Start()
{
    var resolutions : Resolution[] = Screen.resolutions;
    // Print the resolutions
    for (var res in resolutions) {
        print(res.width + "x" + res.height);
    }
    // Switch to the lowest supported fullscreen resolution
  //  Screen.SetResolution (resolutions[0].width, resolutions[0].height, true);
};
﻿using UnityEngine;
using System.Collections;

public class ChildnotEffectByparent : MonoBehaviour {
	float height;
	float width;
	public float s = 1.0f;
	public Transform children = null;
	public Transform child2;
	
	
	// We build an array with all children, so we can re attach them later
	void Awake () 
	{
		height = (float)Camera.main.orthographicSize* 2.0f;
		width = height* Screen.width / Screen.height;

		//children = new Transform[ transform.childCount ];
		int i = 0;  
		//foreach( Transform T in transform )
		//	children[i++] = T;
		children.parent = null;
		//transform.DetachChildren();                     // Detach
		transform.localScale = new Vector3(width/9.9f, 1.0f, height/9.9f);
		//foreach( Transform T in children )              // Re-Attach
			children.parent = transform;
	}

	void Start(){
		children.position = child2.position;

	}
	// Update is called once per frame
	void Update () 
	{
		//if( s != transform.localScale.x )
		//{
			
			//transform.localScale = new Vector3( s, s, s );  // Scale        
			
		//}
	}

}

﻿using UnityEngine;
using System.Collections;

public class MyInputFieldCheck : MonoBehaviour {
	public string inputText = "text";
	private TouchScreenKeyboard keyboard;
	// Updates button's text while user is typing
	void OnGUI() {
		if (GUI.Button(new Rect(0, 10, 200, 32), inputText))
			keyboard = TouchScreenKeyboard.Open(inputText);
		
		if (keyboard != null)
			inputText = keyboard.text;
		
	}
}

﻿#pragma strict

function Update () {
  if(Input.touchCount > 0)
  {
    var hit : RaycastHit; 
    /** position in pixels **/
   var finger = Vector3(Input.GetTouch(0).position.x,
                            Input.GetTouch(0).position.y, 10.0f); //fix z here  
    var ray : Ray = Camera.main.ScreenPointToRay(finger);
    var touchPosition: Vector3;
    var position: Vector3;
    var touch : Touch;
    touch = Input.GetTouch(0);

    /** For debugging, user your own game object name **/        
    Debug.Log("Paddle Position " + GameObject.Find("Paddle").transform.position);            
    if (Physics.Raycast(ray, hit, Mathf.Infinity)) { // hit an object

       if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved){ //finger is moving

         /** position is game world **/                             
         position = Camera.main.ScreenToWorldPoint(finger); 

         /** if you want a trailing object: z is 0 to 1.0 **/
         touchPosition =  Vector3.Lerp(hit.rigidbody.position, position, 0.9f);

         /** Object that was hit **/
         hit.rigidbody.transform.position = touchPosition; 

         /** Debugging **/
         Debug.Log("finger " + finger);
         Debug.Log("position " + position);
         Debug.Log("touchPosition " + touchPosition);
         Debug.Log("paddle " +  hit.rigidbody.position);
        }
    }
  }
}
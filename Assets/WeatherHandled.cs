﻿using UnityEngine;
using System.Collections;

public class WeatherHandled : MonoBehaviour {
	public CloudAnim cloud;
	public RainAnim rain;
	public Snowanim snow;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetString ("Weather") == "Sunny" || PlayerPrefs.GetString("Weather") == "Day") {
			cloud.enabled = true;
			rain.enabled = false;
			snow.enabled = false;
			
		}
		if (PlayerPrefs.GetString ("Weather") == "Rainny") {
			cloud.enabled = false;
			rain.enabled = true;
			snow.enabled = false;
			
		}
		if (PlayerPrefs.GetString ("Weather") == "Snow") {
			cloud.enabled = false;
			rain.enabled = false;
			snow.enabled = true;
			
		}
	
	}
	
	// Update is called once per frame
	void Update () {


	
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetActiveButton : MonoBehaviour {
	public Button sunnyButton;
	public Button rainnyButton;
	public Button snowButton;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		string weather = PlayerPrefs.GetString ("Weather");
		switch (weather) {
		case "Sunny":
		case "Day":
			sunnyButton.Select ();
			break;
		case "Snow":
			snowButton.Select ();
			break;
		case "Rainny":
			rainnyButton.Select ();
			break;
		}
	}
}

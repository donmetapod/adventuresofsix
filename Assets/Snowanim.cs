﻿using UnityEngine;
using System.Collections;

public class Snowanim : MonoBehaviour {
	public Texture[] frames;
	public const int framesPerSecond= 2;
	
	void  Update (){
		int index = (int)((Time.time * framesPerSecond) % frames.Length);
		GetComponent<Renderer>().material.mainTexture = frames[index];
	}
}

﻿using UnityEngine;
using System.Collections;

public class CameraZoomSmooth : MonoBehaviour {
	private float startTouchMagnitude;
	private float startTouchZoom;
	private float targetZoom;
	public float minZoom = 2.0f;
	public float maxZoom = 5.0f;
	public float zoomSpeed = 0.2f;

	void Start()
	{
		print("zoom object " + transform.name);
	}

	void Update() {
		if(Input.touchCount == 2) { // only check when 2 fingers are pressed
			if(Input.touches[1].phase==TouchPhase.Began) { // check that the second finger just began touching
				// remember the distance between the two fingers
				startTouchMagnitude = (Input.touches[0].position-Input.touches[1].position).magnitude;
				// remember the current zoom/orthogrphic size of the camera, we use this to interpolate the zoom smoothly
				startTouchZoom = Camera.main.orthographicSize;
			}
			// calculate the relative change since last frame
			float relativeMagnitudeChange = startTouchMagnitude / (Input.touches[0].position-Input.touches[1].position).magnitude;
			targetZoom = startTouchZoom * relativeMagnitudeChange;
			// Limit the zoom between min and max
			targetZoom = Mathf.Clamp(targetZoom, minZoom, maxZoom);
			print("taget zoom: " + targetZoom);
			// smoothly zoom in and out
			Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, targetZoom, zoomSpeed);
		}

	}
	


}

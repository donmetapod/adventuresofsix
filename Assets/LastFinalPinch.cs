﻿using UnityEngine;
using System.Collections;

public class LastFinalPinch : MonoBehaviour {
	public bool zoomOut = false;
	//bool isMousePressed = false;
	public bool zoomIn = false;
	public bool resetPos = false;
	public int speed = 4;
	public Camera selectedCamera;
	public float MINSCALE = 2.0F;
	public float MAXSCALE = 5.0F;
	public float minPinchSpeed = 5.0F;
	public float varianceInDistances = 5.0F;
	private float touchDelta = 0.0F;
	private Vector2 prevDist = new Vector2(0,0);
	private Vector2 curDist = new Vector2(0,0);
	private float speedTouch0 = 0.0F;
	private float speedTouch1 = 0.0F;
	public bool midPinch = false;
	
	// Use this for initialization
	void Start () 
	{
		
	}

	void Check()
	{
		zoomIn = true;
	}

	void Reset()
	{
		resetPos = true;
	}

	// Update is called once per frame
	void Update () 
	{
		
		if (Input.touchCount == 2 && Input.GetTouch(1).phase == TouchPhase.Moved) 
		{
			
			curDist = Input.GetTouch(0).position - Input.GetTouch(1).position; //current distance between finger touches
			prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition)); //difference in previous locations using delta positions
			touchDelta = curDist.magnitude - prevDist.magnitude;
			speedTouch0 = Input.GetTouch(0).deltaPosition.magnitude / Input.GetTouch(0).deltaTime;
			speedTouch1 = Input.GetTouch(1).deltaPosition.magnitude / Input.GetTouch(1).deltaTime;
			
			
			if ((touchDelta + varianceInDistances <= 1) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
			{
				
				selectedCamera.fieldOfView = Mathf.Clamp(selectedCamera.fieldOfView + (1 * speed),36,60);
			}
			
			if ((touchDelta +varianceInDistances > 1) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
			{
				
				selectedCamera.fieldOfView = Mathf.Clamp(selectedCamera.fieldOfView - (1 * speed),36,60);
			}

			
		}   

		if(selectedCamera.fieldOfView == 60f)
		{
			zoomIn = false;
			//				zoomOut = true;
			//				this.transform.position = new Vector3(47.35f, 20.96f, 22.69f);
		}
		if(selectedCamera.fieldOfView >= 37f)
		{
//			
			//				zoomOut = true;
			//				this.transform.position = new Vector3(47.35f, 20.96f, 22.69f);
		}
		else{
			resetPos = false;
		}
		if (selectedCamera.fieldOfView == 36f) {
			Invoke ("Check", 0.5f);
		} else {
			zoomIn = false;
		}
//		if (selectedCamera.fieldOfView >= 45 && selectedCamera.fieldOfView <= 52) {
//			midPinch = true;
//		} else {
//			midPinch = false;
//		}
	}
}
﻿using UnityEngine;
using System.Collections;

public class ReproducirOGVEnLocalStandalonePlane : MonoBehaviour {

	#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
	public		MovieTexture	peliculaPlane;
				AudioSource		sonido;
				bool			bPlaying	= false;

	void Start()
	{		
		GetComponent<MeshRenderer> ().material.mainTexture = peliculaPlane as MovieTexture;
		sonido = GetComponent<AudioSource>();
		sonido.clip = peliculaPlane.audioClip;
	}

	void Update()
	{		
		if (Input.GetKeyDown ("space")) 
		{			
			if (!bPlaying) {
				bPlaying = true;
			}
			if (bPlaying) {
				if (peliculaPlane.isPlaying) {
					peliculaPlane.Pause ();
				} else {
					peliculaPlane.Play ();
					sonido.Play ();
				}
			} 
		}
	}
	#endif
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReproducirOGVEnLocalStandaloneRawImage : MonoBehaviour {

	#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
	public		MovieTexture	peliculaRaw;
				AudioSource		sonido;
				bool			bPlaying	= false;

	void Start()
	{		
		GetComponent<RawImage> ().texture = peliculaRaw as MovieTexture;
		sonido = GetComponent<AudioSource>();
		sonido.clip = peliculaRaw.audioClip;
	}

	void Update()
	{		
		if (Input.GetKeyDown ("space")) 
		{			
			if (!bPlaying) {
				bPlaying = true;
			}
			if (bPlaying) {
				if (peliculaRaw.isPlaying) {
					peliculaRaw.Pause ();
				} else {
					peliculaRaw.Play ();
					sonido.Play ();
				}
			} 
		}
	}
	#endif
}

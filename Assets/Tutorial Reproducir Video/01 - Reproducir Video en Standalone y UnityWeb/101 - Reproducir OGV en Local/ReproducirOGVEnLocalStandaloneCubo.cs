﻿using UnityEngine;
using System.Collections;

public class ReproducirOGVEnLocalStandaloneCubo : MonoBehaviour {

	#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
	public		MovieTexture	peliculaCubo;
				AudioSource		sonido;
				bool			bPlaying	= false;

	void Start()
	{		
		GetComponent<MeshRenderer> ().material.mainTexture = peliculaCubo as MovieTexture;
		sonido = GetComponent<AudioSource>();
		sonido.clip = peliculaCubo.audioClip;
	}

	void Update()
	{		
		if (Input.GetKeyDown ("space")) 
		{			
			if (!bPlaying) {
				bPlaying = true;
			}
			if (bPlaying) {
				if (peliculaCubo.isPlaying) {
					peliculaCubo.Pause ();
				} else {
					peliculaCubo.Play ();
					sonido.Play ();
				}
			} 
		}
	}
	#endif
}

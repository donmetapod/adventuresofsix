﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReproducirOGVEnServerStandaloneMesh : MonoBehaviour {

	#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
	public		string				urlVideo		= "http://unityinstitute.esy.es/TutorialAssets/CosmosReel.ogv";
	public		MovieTexture		movieTexture;
	public		Text				progresion;
	protected 	bool				streamReady		= false;
				AudioSource			sonido;

	void Start()
	{
		// No queremos que salgan numeros en el porcentaje hasta que no se empiece a bajar el video
		progresion.text = "";
	}

	void Update()
	{
		if (Input.GetKeyDown ("space") && !streamReady) {
			StartCoroutine (StartStream (@urlVideo));
		}
	}

	protected IEnumerator StartStream (string url)
	{
		WWW videoStreamer = new WWW (url);

		// Mientras baja el archivo, se muesta el porcentaje de la progresion
		while (videoStreamer.isDone == false) {
			if(progresion != null) progresion.text = (int)(100.0f * videoStreamer.progress) + "%";
			yield return 0;
		}

		// Metemos el video dentro de la MovieTexture hasta que este lista para su reproduccion
		// Aqui Unity tirara un error, pero es un bug que esta semi arreglado, ya que el video y el audio se reproducen 
		// correctamente, pero la consola indica un error
		movieTexture = videoStreamer.GetMovieTexture();
		while (!movieTexture.isReadyToPlay) {
			yield return 0;
		}

		// Una vez tenemos toda la informacion, preparamos los componentes para su reproduccion
		sonido = GetComponent<AudioSource> ();
		sonido.clip = movieTexture.audioClip;
		// Esta es la unica linea que cambia entre RawImages y Meshes
		GetComponent<MeshRenderer> ().material.mainTexture = movieTexture as MovieTexture;

		// Cuando todo esta listo para su reproduccion, reproducimos el video y borramos el porcentaje
		if (videoStreamer.isDone) {
			progresion.text = "";
			sonido.Play ();
			movieTexture.Play ();
			streamReady = true;
		}
	}
	#endif
}

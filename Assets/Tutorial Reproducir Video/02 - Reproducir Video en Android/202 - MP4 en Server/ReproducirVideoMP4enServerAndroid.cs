﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class ReproducirVideoMP4enServerAndroid : MonoBehaviour {

	#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY

	private 		Color 							bgColor;
	private 		FullScreenMovieControlMode 		controlMode;
	private 		FullScreenMovieScalingMode 		scalingMode;
	private string 									videoFile;
	public			Text							progresion;

	void Start()
	{
		bgColor = Color.black;
		controlMode = FullScreenMovieControlMode.Full;
		scalingMode = FullScreenMovieScalingMode.AspectFill;
		videoFile = Application.persistentDataPath + "/video.mp4";
	}

	IEnumerator DownloadAndPlayVideo () 
	{
		string playVideoFile = videoFile;

		playVideoFile = "file://" + videoFile;

		// Habilitar para server
		//WWW www = new WWW("http://unityinstitute.esy.es/TutorialAssets/CosmosReelMP.mp4");
		// Habilitar para Youtube
		WWW www = new WWW("https://r8---sn-w511uxa-cjol.googlevideo.com/videoplayback?dur=342.796&sver=3&source=youtube&pl=20&id=o-AJF3OCEnfemvjgv0pdluzSjFF0n92jKM27rpNTpaTa1g&ms=au&mt=1467170436&mv=m&sparams=dur%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpcm2cms%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&signature=9D14843B4A95D2E62ECC19DDFDE1F785449C63FB.8EFAAFF709C49420DCED0AD87D0AE7EC2F3A8A35&mn=sn-w511uxa-cjol&key=yt6&ip=188.77.177.183&lmt=1466577968704662&upn=-LEg_aH3CMA&itag=18&fexp=9405330%2C9416126%2C9416778%2C9416891%2C9418642%2C9419452%2C9422596%2C9428398%2C9431012%2C9432367%2C9433096%2C9433223%2C9433946%2C9434183%2C9435526%2C9435876%2C9436085%2C9436982%2C9437066%2C9437259%2C9437403%2C9437553%2C9438139%2C9438256%2C9438662%2C9439438%2C9439535%2C9439652%2C9439683%2C9440135%2C9440501&expire=1467192237&ratebypass=yes&ipbits=0&requiressl=yes&mime=video%2Fmp4&initcwndbps=1602500&pcm2cms=yes&mm=31");

		while (www.isDone == false) {
			if(progresion != null) progresion.text = (int)(100.0f * www.progress) + "%";
			yield return 0;
		}
		yield return www;
		if (www != null && www.isDone && www.error == null) 
		{
			FileStream stream = new FileStream(videoFile, FileMode.Create);
			stream.Write(www.bytes, 0, www.bytes.Length);
			stream.Close();
		}
		Handheld.PlayFullScreenMovie(playVideoFile, bgColor, controlMode, scalingMode);
	}

	public void BotonSalir()
	{
		Application.Quit ();
	}

	public void BotonReproducirVideo()
	{
		StartCoroutine (DownloadAndPlayVideo());
	}

	#endif
}

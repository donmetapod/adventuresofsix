﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class ReproducirVideoYoutubeAndroid : MonoBehaviour {

	#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY

	private 		Color 							bgColor;
	private 		FullScreenMovieControlMode 		controlMode;
	private 		FullScreenMovieScalingMode 		scalingMode;
	private string 									videoFile;
	public			Text							progresion;

	void Start()
	{
		bgColor = Color.black;
		controlMode = FullScreenMovieControlMode.Full;
		scalingMode = FullScreenMovieScalingMode.AspectFill;
		videoFile = Application.persistentDataPath + "/video.mp4";
	}

	IEnumerator DownloadAndPlayVideo () 
	{
		string playVideoFile = videoFile;

		playVideoFile = "file://" + videoFile;

		// Habilitar para server
		//WWW www = new WWW("http://unityinstitute.esy.es/TutorialAssets/CosmosReelMP.mp4");
		// Habilitar para Youtube
		WWW www = new WWW("https://r7---sn-w511uxa-cjoe.googlevideo.com/videoplayback?itag=18&fexp=9407157%2C9416126%2C9416891%2C9419451%2C9422596%2C9428398%2C9431012%2C9431685%2C9432564%2C9433096%2C9433380%2C9433630%2C9433946%2C9434803%2C9435409%2C9435525%2C9435526%2C9435667%2C9435876%2C9436811%2C9436929%2C9437066%2C9437145%2C9437553%2C9437948%2C9438327%2C9438786%2C9439652&key=yt6&upn=dPPAvj9XH1M&ratebypass=yes&ipbits=0&sver=3&initcwndbps=1527500&signature=AE959EC4C9A094ED917FD231BCD299E4DD0F48DC.9BF1660F857C9800B852FB3F62A3B0FE0E4E08C1&dur=2010.058&pcm2=yes&sparams=dur%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpcm2%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&lmt=1465774248575672&mt=1466989839&mv=m&id=o-AELzq8qbVcP4NBQ73XGYKCc6KPE2roJVe4-Nxk1nGNz3&ms=au&expire=1467011686&mime=video%2Fmp4&ip=188.77.177.183&requiressl=yes&mm=31&source=youtube&pl=20&mn=sn-w511uxa-cjoe");

		while (www.isDone == false) {
			if(progresion != null) progresion.text = (int)(100.0f * www.progress) + "%";
			yield return 0;
		}
		yield return www;
		if (www != null && www.isDone && www.error == null) 
		{
			FileStream stream = new FileStream(videoFile, FileMode.Create);
			stream.Write(www.bytes, 0, www.bytes.Length);
			stream.Close();
		}
		Handheld.PlayFullScreenMovie(playVideoFile, bgColor, controlMode, scalingMode);
	}

	public void BotonSalir()
	{
		Application.Quit ();
	}

	public void BotonReproducirVideo()
	{
		StartCoroutine (DownloadAndPlayVideo());
	}

	#endif
}

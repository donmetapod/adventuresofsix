﻿using UnityEngine;
using System.Collections;

public class ReproducirVideoMP4enLocalAndroid : MonoBehaviour {

	#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
	private string movPath = "UDKModularMap.mp4";

	public void BotonReproducirVideo()
	{
		StartCoroutine(PlayStreamingVideo(movPath));
	}

	public void BotonSalir()
	{
		Application.Quit ();
	}

	private IEnumerator PlayStreamingVideo(string url)
	{
		Handheld.PlayFullScreenMovie(url, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFill);
		yield return new WaitForEndOfFrame();

		Debug.Log("Video playback completed.");
	}
	#endif
}

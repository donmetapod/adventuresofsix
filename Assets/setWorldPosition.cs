﻿using UnityEngine;
using System.Collections;

public class setWorldPosition : MonoBehaviour {
	public GameObject other;
	// Use this for initialization
	void Start () {
	 var worldPosition = transform.position + transform.parent.transform.position;
		other.transform.position = worldPosition;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
